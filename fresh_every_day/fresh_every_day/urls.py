"""fresh_every_day URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^user/',include(('fresh_user.urls','fresh_user'),namespace='user')), #用户模块
    url(r'^cart/',include(('fresh_cart.urls','fresh_cart'),namespace='cart')), #购物车模块
    url(r'^order/',include(('fresh_order.urls','fresh_order'),namespace='order')), #订单模块
    url(r'^',include(('fresh_goods.urls','fresh_goods'),namespace='goods')),#商品模块
    path('captcha',include('captcha.urls')), #图形验证码
    url(r'^tinymce/',include('tinymce.urls')), #富文本编辑器
    url(r'^search',include('haystack.urls')) #全文检索框架
]
