#统一接口模块restful.py
from django.http import JsonResponse
from aliyunsms import *
class HttpCode(object):
    ok=200
    paramserror=400
    methoderror=404
    servererror=500

def result(code=HttpCode.ok,message='',data=None,kwargs=None):
    json_dict={
        'code':code,
        'message':message,
        'result':data
    }
    #isinstance()函数来判断一个对象是否是一个已知类型，类似type().判断他们类型是否相同，结果用布尔
    if kwargs and isinstance(kwargs,dict) and kwargs.keys():
        json_dict.update(kwargs)
    return JsonResponse(json_dict,json_dumps_params={'ensure_ascii':False})

def ok(message,data=None):
    return result(code=HttpCode.ok,message=message,data=data)

def params_error(message="",data=None):
    return result(code=HttpCode.paramserror,message=message,data=data)

def method_error(message="",data=None):
    return result(code=HttpCode.methoderror,message=message,data=data)

def server_error(message="",data=None):
    return result(code=HttpCode.servererror,message=message,data=data)

if __name__=='__main__':
    #调用下发短信方法
    send_sms('18636350081',get_code(6,False))
    print(get_code(6,False)) #打印6位数字验证码
    print(get_code(6,True)) #打印6位数字字母混合验证码
    print(get_code(4,False))
    print(get_code(4,True))
