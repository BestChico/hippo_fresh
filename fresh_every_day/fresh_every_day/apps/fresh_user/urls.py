from django.contrib.auth.decorators import login_required
from django.conf.urls import url,include
from django.urls import path
from .views import  *
"""
login_required() --django内置装饰器
判断用户是否登陆
登录--执行括号中的视图函数
未登录--重定向到含有next的页面 如 ：http://127.0.0.1:8000/accounts/login/?next=/user/
"""
urlpatterns=[
# url(r'^',send), #测试邮件发送
    url(r'^register$',Register.as_view(),name='register'), #注册
    url(r"^active/(?P<token>.*)$", Active.as_view(),name='active'), #激活邮件
    url(r'^login$',Login.as_view(),name='login'), #登录
    url(r'^logout$',LoginoutView.as_view(),name='logout'),#退出登录
    path('sms_send/<phone>/', sms_send, name="sms_send"),#短信发送
    path('sms_check/',sms_check, name="sms_check"),#短信验证
    path('img_refresh/',img_refresh,name='img_refresh'),#图形验证码刷新
    path('img_check/',img_check,name='img_check'),#图形验证码验证

    # url(r'^$',login_required(UserInfoView.as_view()),name='user'), #用户中心--信息页
    # url(r'^order$',login_required(UserOrderView.as_view()),name='order'), #用户中心--订单页
    # url(r'^address$',login_required(AddressView.as_view()),name='address') #用户中心--地址页
    url(r'^$',UserInfoView.as_view(),name='user'), #用户中心--信息页
    url(r'^order/(?P<page>\d+)$',UserOrderView.as_view(),name='order'), #用户中心--订单页
    url(r'^address$',AddressView.as_view(),name='address') #用户中心--地址页
]