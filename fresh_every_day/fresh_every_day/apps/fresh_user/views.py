from django.shortcuts import render, redirect, reverse, HttpResponse
from django.http import JsonResponse
from django.views.generic.base import TemplateView
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from celery_tasks.tasks import send_register_active_email
from django.contrib.auth import authenticate, login,logout
from fresh_every_day import settings
from fresh_order.models import *
from itsdangerous import SignatureExpired
from captcha.models import CaptchaStore
from captcha.fields import CaptchaField
from fresh_goods.models import *
from django.core.paginator import Paginator
from captcha.helpers import captcha_image_url
from django_redis import get_redis_connection
from utlis.mixin import  *
from message_check import restful
from django.core.mail import send_mail
from django.views.generic import View
from django.core.cache import cache
from .forms import *
from .models import *
import random
import aliyunsms  # 封装好的发送验证码模块
import json
# Create your views here.
import re


# 用户注册  邮箱未激活
class Register(View):
    def get(self, request):
        return render(request, 'register.html')

    def post(self, request):
        username = request.POST.get("user_name")
        password = request.POST.get("pwd")
        email = request.POST.get("email")
        allow = request.POST.get("allow")
        if not all([username, password, email]):
            error_msg = '数据不完整'
            return render(request, 'register.html', locals())
        # 判断用户填写的邮箱是否为正确邮箱
        if not re.match(r"^[a-z0-9][\w.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$", email):
            error_msg = '邮箱格式不正确'
            return render(request, 'register.html', locals())
        # 判断用户注册时是否勾选同意，同意后该字段POST请求参数为o
        if allow != "on":
            error_msg = '请勾选同意'
            return render(request, 'register.html', locals())
        print(username, password, email)
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            """如果出现该异常说明用户名不存在，则让user对象为空"""
            user = None
        if user:
            error_msg = '用户名已经存在'
            return render(request, 'register.html', locals())
        user = User.objects.create_user(username, email, password)
        user.is_active = 0
        user.save()
        serializer = Serializer(settings.SECRET_KEY, 3600)  # 有效期1小时
        info = {"confirm": user.id}
        token = serializer.dumps(info)
        token = token.decode()
        subject = '即速生鲜'  # 邮件标题
        message = '欢迎加入我们即速生鲜'  # 邮件正文
        sender = settings.EMAIL_FROM
        receiver = [email]
        html_message = """           <h1>%s  恭喜您成为海马生鲜注册会员</h1><br/><h3>请您在1小时内点击以下链接进行账户激 活</h3><a href="http://127.0.0.1:8000/user/active/%s">http://127.0.0.1:8000/user/active/%s</a> """ % (
            username, token, token)
        # send_mail(subject,message,sender,receiver,html_message=html_message)
        send_register_active_email(email, username, token)
        return redirect(reverse('goods:home'))


# 邮箱发送 测试设置是否成功
# send_mail的参数分别是  邮件标题，邮件内容，发件箱(settings.py中设置过的那个)，##收件箱列表(可以发送给多个人),失败静默(若发送失败，报错提示我们)
# def send(request):
#     send_mail('李昱龙你好','你好','1444587467@qq.com',['1198558613@qq.com'],fail_silently=False)
#     return HttpResponse('ok')

# 用户注册激活
class Active(View):
    def get(self, request, token):
        serializer = Serializer(settings.SECRET_KEY, 3600)
        try:
            info = serializer.loads(token)  # 获取用户id    
            user_id = info['confirm']  # 根据用户id 获取该用户对象    
            user = User.objects.get(id=user_id)  # 设置该用户对象中的is_active字段的值为1    
            user.is_active = 1
            user.save()  # 使用反向解析跳转到登录页    
            return redirect(reverse("user:login"))
        except SignatureExpired as e:  # 出现异常表示链接失效    
            return HttpResponse("激活链接已过期")


# 用户登录
class Login(View):
    def get(self, request):
        # 判断是否记住了用户名
        if 'username' in request.COOKIES:
            username = request.COOKIES.get('username')
            checked = 'checked'
        else:
            username = ''
            checked = ''
        login_form = LoginForm()
        # 图形验证码
        # hashkey验证码生成的密钥，image_url验证码的图片地址
        hashkey = CaptchaStore.generate_key()

        # image_url=captcha_image_url(hashkey)
        # Python内置了一个locals()他会返回本地变量的所有变       量字典
        #
        return render(request, 'login.html', locals())

    def post(self, request):
        login_form = LoginForm(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('pwd')
        print(username,password)
        if not all([username, password]):
            error_msg = '数据不完整'
            return render(request, 'login.html', locals())
        # 验证码错误
        if not login_form.is_valid():
            error_msg = '验证码错误'
            return render(request, 'login.html', locals())
        # 校验用户名密码，当用户名密码正确情况下保存登录状态到session中，使用django认证系统中的 authenticate和login方法
        user = authenticate(username=username, password=password)  # 正确返回user对象不正确返回None
        if user is not None:
            # 用户密码正确
            if user.is_active:  # 用户已激活
                # 使用django的login将user对象保存到session中
                login(request, user)
                # 返回主页
                #获取登录后用户要跳转的地址--默认跳转到首页
                #当get获取到了next 机会返回next  如果获取的是none 就会返回reverse('goods:home')的值
                next_url=request.GET.get('next',reverse('goods:home'))
                # 实现页面的跳转
                response = redirect(next_url)
                # 判断是否需要记住用户名
                remember = request.POST.get('remember')
                print(remember, username, password)
                if remember == 'on':
                    # 记住用户名
                    response.set_cookie('username', username, max_age=7 * 21 * 3600)
                else:
                    response.delete_cookie('username')
                return response
            else:  # 账户未激活
                error_msg = '账户未激活'
                return render(request, 'login.html', locals())
        else:  # 用户名或密码错误
            error_msg = '用户名或密码错误'
            return render(request, 'login.html', locals())


# 图形验证码刷新-是否为ajax请求刷新验证码
def img_refresh(request):
    print('refresh_image')
    # is_ajax()会判断请求头里‘HTTP_X_REQUESTED_WITH'的值。
    # 如果请求方式不是ajax，那么请求头里不含'HTTP_X_REQUESTED_WITH'的，
    # 如果是ajax请求，is_ajax()则会返回True
    if not request.is_ajax():
        return HttpResponse('不是ajax请求')
    new_kew = CaptchaStore.generate_key()
    to_json_reponse = {
        'hashkey': new_kew,
        'image_url': captcha_image_url(new_kew)
    }
    return HttpResponse(json.dumps(to_json_reponse))


# 判断输入的验证码是否于生成的验证码匹配
def img_check(request):
    print('判断中。。。')
    if request.is_ajax():
        print(request.GET.get('code'))  # 获取到输入框中输入的值
        challenge = CaptchaStore.objects.get(hashkey=request.GET.get('hashkey')).challenge
        print(challenge)
        response = CaptchaStore.objects.get(hashkey=request.GET.get('hashkey')).response
        print(response)
        if request.GET.get('code') == challenge or request.GET.get('code') == response:
            data = {'status': 1}
        else:
            data = {'status': 0}
        return JsonResponse(data)


# #发送短信的接口
def sms_send(request, phone):
    code = aliyunsms.get_code(6, False)  # 发送一个纯数字的验证码
    cache.set(phone, code, 60)  # 设置有效期为60s
    print('判断缓存中是否有，', cache.has_key(phone))
    print('获取Redis的验证码，', cache.get(phone))
    print('-' * 25)
    print(phone)
    print(type(phone))
    print('-' * 25)
    result = aliyunsms.send_sms(phone, code)
    return HttpResponse(result)


# #短信验证校验码
def sms_check(request):
    phone = request.GET.get('phone')
    code = request.GET.get('code')
    print('判断缓存中是否有:', cache.has_key(phone))
    print('获取Redis验证码:', cache.get(phone))

    cache_code = cache.get(phone)
    if cache_code == code:
        # print('ok')
        # return HttpResponse(json.dumps({'result':'OK'}))
        print(1)
        return restful.ok('OK', data=None)
    else:
        # print('bo')
        # return HttpResponse(json.dumps({'result':'False'}))
        print(2)
        return restful.params_error('验证码错误', data=None)

#用户中心信息页面
class UserInfoView(LoginRequiredMixin,View):
    def get(self,request):
        page='user'
        # request.user.is_authenticated()
        #如果用户登录 -->User类的一个实例 --True
        #如果用户未登录-->AnonymouseUser的一个实例 --False
        #除了你给模版传递的模版变量，django框架会把request.user也传给模版文件

        #获取用户的个人信息
        user=request.user
        address=Address.objects.get_default_address(user)
        # #获取用户的地址浏览记录
        # from redis import StrictRedis
        # #创建Redis对象
        # sr=StrictRedis(host='172.20.10.2',port='6379',db=5)
        con=get_redis_connection('default') #Redis对象

        #取出用户浏览记录
        history_key='history_%d'%user.id
        #获取用户最新浏览的5个商品的id
        sku_ids=con.lrange(history_key,0,4) #返回一个id列表

        # #从数据库中查询用户浏览的商品具体信息
        # goods_li=GoodsSKU.objects.filter(id_in=sku_ids)
        # #由于in查询是根据数据库是否存在且返回的形式是数据库中数据的先后顺序 所以要对数据进行处理才可以
        # good_res=[]
        # for a_id in sku_ids:
        #     for goods in goods_li:
        #         if a_id==goods.id:
        #             good_res.append(goods)

        #遍历用户浏览的历史商品信息
        goods_li=[]
        for id in sku_ids:
            goods_li.append(GoodsSKU.objects.get(id=id))

        return render(request,'user_center_info.html',locals())

#用户中心订单页面
class UserOrderView(LoginRequiredMixin,View):
    def get(self,request,page):
        #显示
        #获取用户订单信息
        user=request.user
        orders=OrderInfo.objects.filter(user=user).order_by('-create_time')

        #遍历order获取商品信息
        for order in orders:
            #根据order_id查询订单商品信息
            order_skus=OrderGoods.objects.filter(order_id=order.order_id)

            #遍历order_skus计算每个商品的小计
            for order_sku in order_skus:
                #计算小计
                amount=order_sku.count*order_sku.price
                #动态给order_sku增加属性amount,保存订单商品的小计
                order_sku.amount=amount
            #动态给order增加属性，保存订单的状态
            #将models中定义的字典赋值给对应的状态
            order.status_name=OrderInfo.ORDER_STATUS[order.order_status]
            #动态给order增加属性，保存订单商品的信息
            order.order_skus=order_skus

        #分页
        paginator=Paginator(orders,1)

        #对页码进行处理
        try:
            page = int(page)
        except Exception as e:
            page = 1
        # 如果输入的页码大于总页数 默认跳转到第一页
        # Paginator.num_pages 获取总页数
        if page > paginator.num_pages:
            page = 1

        # 获取第page页的page示例对象
        # Paginator.page() 返回一个page的示例对象并包含了当前页的数据
        order_page = paginator.page(page)

        # 进行页码的控制 页面上最多显示5个页码
        # 考虑情况
        # 1 如果总页数小于5页  -- 页面上显示所有页码
        # 2 如果当前页是前3页，显示1-5页的页码
        # 3 如果当前页码是后3页，显示后5页的页码
        # 4 其他情况 显示当前页的前两页，当前页的后两页
        num_pages = paginator.num_pages # 获取页码总数
        if num_pages < 5:
            pages = range(1, num_pages + 1)
        elif page <= 3:
            pages = range(1, 6)
        elif num_pages - page <= 2:
            pages = range(num_pages - 4, num_pages + 1)
        else:
            pages = range(page - 2, page + 3)
        return render(request,'user_center_order.html',locals())


#用户中心地址页面
class AddressView(LoginRequiredMixin,View):
    def get(self,request):
        page='address'
        #获取用户的默认收货地址
        user = request.user  # 获取登录用户对应的User对象
        # try:
        #     address = Address.objects.get(user=user, is_default=True)
        # except Address.DoesNotExist:
        #     # 不存在默认收货地址
        #     address = None
        address=Address.objects.get_default_address(user)
        return render(request,'user_center_site.html',locals())
    #地址的添加
    def post(self,request):
        #接受提交数据
        receiver=request.POST.get('recevier')
        addr=request.POST.get('addr')
        zip_code=request.POST.get('zip_code')
        phone=request.POST.get('phone')
        #校验数据
        if not all([receiver,addr,phone]):
            error_msg='数据不完整'
            return render(request,'user_center_site.html',locals())
        #校验是否为有效手机号
        if not re.match(r'^1[3|4|5|7|8][0-9]{9}$',phone):
            error_msg='手机号不合法'
            return render(request,'user_center_site.html',locals())
        #业务处理 地址添加
        #如果用户已存在默认收货地址，添加的地址不作为默认收货地址，否则作为默认收货地址
        user=request.user #获取登录用户对应的User对象
        # try:
        #     address=Address.objects.get(user=user,is_default=True)
        # except Address.DoesNotExist:
        #     #不存在默认收货地址
        #     address=None
        address = Address.objects.get_default_address(user)
        if address:
            is_default=False
        else:
            is_default=True
        #添加地址
        Address.objects.create(user=user,
                                receiver=receiver,
                               addr=addr,
                               zip_code=zip_code,
                               phone=phone,
                               is_default=is_default)
        #返回应答,刷新地址页面
        return redirect(reverse('user:address'))

#退出登录
class LoginoutView(View):
    def get(self,request):
        #退出登录 --清除用户的session信息
        logout(request)
        #跳转到首页
        return redirect(reverse('goods:home'))
