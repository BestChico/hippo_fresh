from django.apps import AppConfig


class FreshUserConfig(AppConfig):
    name = 'apps.fresh_user'
