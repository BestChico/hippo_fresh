from django.conf.urls import url
from .views import *
urlpatterns=[
    url(r'^home$',HomeView.as_view(),name='home'),
    url(r'^goods/(?P<goods_id>\d+)$',DetailView.as_view(),name='detail'),
    url(r'^list/(?P<type_id>\d+)/(?P<page>\d+)$',ListView.as_view(),name='list')
]