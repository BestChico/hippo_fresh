from django.shortcuts import render, redirect, reverse
from django.views.generic import View
from fresh_goods.models import *
from fresh_order.models import *
from django.core.cache import cache
from django_redis import get_redis_connection
from django.core.paginator import Paginator

# Create your views here.

# 首页
"""
商品信息、首页轮播图商品信息、首页分类商品信息都是每个用户访问时都要加载的内容，这里视图都要进行数据库查询操作，这里就要用django的缓存来将这些都相同的进行缓存，减少数据库的查询操作
"""


class HomeView(View):
    def get(self, request):
        # 尝试从缓存中获取数据
        # cache.delete('index_page_data')
        # print('删除缓存')
        context = cache.get('index_page_data')
        print(context)
        if context is None:
            print('设置缓存')
            # 说明缓存中没有数据--就要进行数据库的查询操作
            # 获取商品首页信息
            types = GoodsType.objects.all()

            # 获取首页轮播图商品信息
            goods_banner = IndexGoodsBanner.objects.all().order_by('index')  # 0,1,2 默认升序

            # 获取首页促销活动信息
            promotion_banners = IndexPromotionBanner.objects.all().order_by('index')  # 0,1,2 默认升序

            # 获取首页分类商品展示信息
            for type in types:  # GoodTypes
                # 获取type种类首页分类商品的图片展示信息
                image_banner = IndexTypeGoodsBanner.objects.filter(type=type, display_type=1).order_by(
                    'index')  # 0,1,2 默认升序
                # 获取type种类首页分类商品的文字展示信息
                title_banners = IndexTypeGoodsBanner.objects.filter(type=type, display_type=0).order_by(
                    'index')  # 0,1,2 默认升序
                # 动态给type增加属性，分别保存首页分类商品的图片展示信息和文字展示信息
                type.image_banners = image_banner
                type.title_banner = title_banners
                # 获取缓存
                # cache.set(key,value,timeout) key、value、过期时间,--timeout可以不填 如果不填 就会默认设置为None 即永远不会过期
                context = {
                    'types': types,
                    'goods_banner': goods_banner,
                    'promotion_banners': promotion_banners
                }
                cache.set('index_page_data', context, 3600)
        # 获取用户购物车中商品的数目
        # 通过登录状态判断是否显示购物车
        user = request.user
        print(user.is_authenticated)
        cart_count = 0
        if user.is_authenticated:
            # 用户已经登陆
            conn = get_redis_connection('default')
            cart_key = 'cart_%d' % user.id
            cart_count = conn.hlen(cart_key)  # heln方法返回hash中元素的个数
        context.update(cart_count=cart_count)
        return render(request, 'index.html', context=context)


# 商品详情页面
# goods/商品id
class DetailView(View):
    def get(self, request, goods_id):
        # 显示详情页
        # 获取商品
        try:
            sku = GoodsSKU.objects.get(id=goods_id)
        except GoodsSKU.DoesNotExist:
            # 商品不存在
            return redirect(reverse('goods:home'))
        # 获取商品的分类信息
        types = GoodsType.objects.all()

        # 获取商品评论信息
        sku_orders = OrderGoods.objects.filter(id=goods_id).exclude(comment='')

        # 获取同一个SPU的其他规格商品
        same_spu_skus = GoodsSKU.objects.filter(goods=sku.goods).exclude(id=goods_id)

        # 获取用户购物车中商品的数目
        # 通过登录状态判断是否显示购物车
        user = request.user
        print(user.is_authenticated)
        cart_count = 0
        if user.is_authenticated:
            # 用户已经登陆
            conn = get_redis_connection('default')
            cart_key = 'cart_%d' % user.id
            cart_count = conn.hlen(cart_key)  # heln方法返回hash中元素的个数

            # 添加用户浏览记录
            conn = get_redis_connection('default')
            history_key = 'history_%d' % user.id
            # 移除列表中的good_id  -- lrem
            conn.lrem(history_key, 0, goods_id)
            # 将good_id插入列表的左侧 -- lpush
            conn.lpush(history_key, goods_id)
            # 保留用户浏览的前5条记录
            conn.ltrim(history_key, 0, 4)
        # 获取新品信息 --根据create_time来排序 降序排列
        new_skus = GoodsSKU.objects.filter(type=sku.type).order_by('-create_time')[:2]
        return render(request, 'detail.html', locals())


# 种类id 页码 排序方式
"""
result api设计风格 : 在用户访问一个地址是，实际上就是请求一种资源
    这里推荐使用第三种格式 种类id为1页码为1的按照人气排序的
"""


# /list?type_id=种类id&page=页码&sort-排序方式
# /list/种类id/页码/排序方式
# /list/种类id/页码?sort=排序方式
class ListView(View):
    # 商品列表页
    def get(self, request, type_id, page):
        # 先获取种类的信息
        try:
            type = GoodsType.objects.get(id=type_id)
        except GoodsType.DoesNotExist:
            # 范湖首页
            return redirect(reverse('goods:home'))
        # 获取商品的分类信息
        types = GoodsType.objects.all()
        # 获取排序方式
        # sort=default 按照默认id排序
        # sort=price 按照默认price排序
        # sort=hot 按照默认销量排序
        # 获取商品分类信息
        sort = request.GET.get('sort')
        if sort == 'price':
            skus = GoodsSKU.objects.filter(type=type).order_by('-price')
        elif sort == 'hot':
            skus = GoodsSKU.objects.filter(type=type).order_by('-sales')
        else:
            sort = 'default'
            skus = GoodsSKU.objects.filter(type=type).order_by('-id')

        # 对数据进行分页
        # Paginator(object_list,per_page)
        # 第一个参数代表传入一个列表，Queryset容器或者其他可以遍历的类型
        # 第二个参数代表每一页显示多少个数据
        paginator = Paginator(skus, 1)
        # 对页码进行容错处理
        # 如果输入的page不是整数 默认跳转到第一页
        try:
            page = int(page)
        except Exception as e:
            page = 1
        # 如果输入的页码大于总页数 默认跳转到第一页
        # Paginator.num_pages 获取总页数
        if page > paginator.num_pages:
            page = 1

        # 获取第page页的page示例对象
        # Paginator.page() 返回一个page的示例对象并包含了当前页的数据
        skus_page = paginator.page(page)

        # 进行页码的控制 页面上最多显示5个页码
        # 考虑情况
        # 1 如果总页数小于5页  -- 页面上显示所有页码
        # 2 如果当前页是前3页，显示1-5页的页码
        # 3 如果当前页码是后3页，显示后5页的页码
        # 4 其他情况 显示当前页的前两页，当前页的后两页
        num_pages = paginator.num_pages # 获取页码总数
        if num_pages < 5:
            pages = range(1, num_pages + 1)
        elif page <= 3:
            pages = range(1, 6)
        elif num_pages - page <= 2:
            pages = range(num_pages - 4, num_pages + 1)
        else:
            pages = range(page - 2, page + 3)
        # 获取用户购物车中商品的数目
        # 通过登录状态判断是否显示购物车
        user = request.user
        print(user.is_authenticated)
        cart_count = 0
        if user.is_authenticated:
            # 用户已经登陆
            conn = get_redis_connection('default')
            cart_key = 'cart_%d' % user.id
            cart_count = conn.hlen(cart_key)  # heln方法返回hash中元素的个数

        # 获取新品信息 --根据create_time来排序 降序排列
        new_skus = GoodsSKU.objects.filter(type=type).order_by('-create_time')[:2]
        return render(request, 'list.html', locals())
