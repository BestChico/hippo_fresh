from django.shortcuts import render, redirect, reverse
from django.views.generic import View
from django.http import *
from fresh_goods.models import *
from fresh_user.models import *
from utlis.mixin import *
from .models import *
from django_redis import get_redis_connection
from datetime import datetime
from django.db import transaction
from alipay import AliPay
import os

# Create your views here.

# 提交订单
# /order/place
class OrderPlaceView(LoginRequiredMixin, View):
    # 提交订单页面显示
    def post(self, request):
        user = request.user
        # 获取参数sku_ids
        sku_ids = request.POST.getlist('sku_ids')
        # 校验参数
        if not sku_ids:
            # 跳转到购物车页面
            return redirect(reverse('cart:show'))

        conn = get_redis_connection('default')
        cart_key = 'cart_%d' % user.id

        skus = []
        total_conut = 0  # 保存商品的总件数
        total_price = 0  # 保存商品的总价格
        # 遍历sku_ids获取用户要购买的商品的信息
        for sku_id in sku_ids:
            # 根据商品的id获取对应商品的信息
            sku = GoodsSKU.objects.get(id=sku_id)
            # 获取用户所要购买的商品数量
            count = conn.hget(cart_key, sku_id)
            # 计算商品的小计
            amount = sku.price * int(count)
            # 动态给sku添加count属性 保存购买商品的数量
            sku.count = int(count)
            # 动态的给sku添加amount属性 保存购买商品的小计
            sku.amount = amount
            # 追加
            skus.append(sku)
            # 累加计算商品的总件数和总价格
            total_conut += int(count)
            total_price += amount

        # 运费：实际开发的时候 属于一个子系统
        transit_price = 10  # 写死

        # 实付款
        total_bay = total_price + transit_price

        # 获取用户的收货地址
        addrs = Address.objects.filter(user=user)

        sku_ids = ','.join(sku_ids)  # [1,25] -->1,25
        print(sku_ids)
        return render(request, 'place_order.html', locals())


# 订单创建
# /order/commit
# 前端需要传递的参数 地址id(addr_id) 支付方式(pay_method) 用户要购买的商品id字符串(sku_ids)
#mysql事务 一组sql操作 要么都成功 要么都失败
#悲观锁
class OrderCommitView1(View):
    # 订单创建
    @transaction.atomic
    def post(self, request):
        # 判断用户是否登录
        user = request.user
        if not user.is_authenticated:
            # 用户未登录
            return JsonResponse({'res': 0, "errmsg": '用户未登录'})
        # 接收参数
        addr_id = request.POST.get('addr_id')
        pay_method = request.POST.get('pay_method')
        sku_ids = request.POST.get('sku_ids')

        # 校验参数
        if not all([addr_id, pay_method, sku_ids]):
            return JsonResponse({'res': 1, 'errmsg': '参数不完整'})

        # 校验支付方式
        if pay_method not in OrderInfo.PAY_METHODS.keys():
            return JsonResponse({'res': 2, 'errmsg': '非法的支付'})

        # 校验地址
        try:
            addr = Address.objects.get(id=addr_id)
        except Address.DoesNotExist:
            return JsonResponse({'res': 3, 'errmsg': '地址非法'})

        # todo:创建订单核心业务
        # 创建订单记录缺少的参数：order_id  total_count  total_price transit_price
        # 组织参数
        # 订单id ： 10771122181630+用戶id
        order_id = datetime.now().strftime('%Y%m%d%H%M%S') + str(user.id)

        # 运费
        transit_price = 10

        # 总数目和总金额
        total_count = 0
        total_price = 0

        #设置事务保存点
        save_id=transaction.savepoint()
        try:
            # todo:向df_order_info表中添加一条记录
            order = OrderInfo.objects.create(order_id=order_id,
                                             user=user,
                                             addr=addr,
                                             pay_method=pay_method,
                                             total_count=total_count,
                                             total_price=total_price,
                                             transit_price=transit_price)
            # todo:用户的订单中有几个商品，需要向df_order_goods表中加入几条记录
            conn = get_redis_connection('default')
            cart_key = 'cart_%d' % user.id

            #由于获取到的sku_ids的格式是  1，25 这里要进行拆分
            sku_ids = sku_ids.split(',')
            for sku_id in sku_ids:
                # 获取商品的信息
                try:
                    #增加悲观锁
                    sku = GoodsSKU.objects.select_for_update().get(id=sku_id)
                except:
                    # 商品不存在
                    #将回滚到保存点的位置 不对数据保存
                    transaction.savepoint_rollback(save_id)
                    return JsonResponse({'res': 4, 'errmsg': '商品不存在'})
                print('user:%d stock%d'%(user.id,sku.stock))
                import time #休眠一下 测试
                time.sleep(10)
                # 从redis中获取用户所需要购买的商品的数量
                count = conn.hget(cart_key, sku_id)

                #todo:判断商品库存
                #当两个用户同时购买一件商品 前一个用户已经吧商品买完库存不足，下一个用户购买时会抱错
                if int(count)>sku.stock:
                    transaction.savepoint_rollback(save_id)
                    return JsonResponse({'res':6,'errmsg':'商品库存不足'})
                # todo:向df_order_goods表中添加一条记录
                OrderGoods.objects.create(order=order,
                                          sku=sku,
                                          count=count,
                                          price=sku.price
                                          )
                # todo:更新商品的库存和销量
                sku.stock -= int(count)
                sku.sales += int(count)
                sku.save()
                # todo:累加计算订单商品的总数目和总价格
                amount = sku.price * int(count)

                total_count += int(count)
                total_price += amount
            # todo:更新订单信息表中的总数量和总价格
            order.total_count = total_count
            order.total_price = total_price
            order.save()
        except Exception as e:
            transaction.savepoint_rollback(save_id)
            return JsonResponse({'res':7,'errmsg':'下单失败'})

        #提交事务
        transaction.savepoint_commit(save_id)
        # todo:清除用户购物车中对应记录 加*会对列表中的所有数进行删除
        conn.hdel(cart_key, *sku_ids)

        # 返回应答
        return JsonResponse({'res': 5, 'message': '创建成功'})
#乐观锁
class OrderCommitView(View):
    # 订单创建
    @transaction.atomic
    def post(self, request):
        # 判断用户是否登录
        user = request.user
        if not user.is_authenticated:
            # 用户未登录
            return JsonResponse({'res': 0, "errmsg": '用户未登录'})
        # 接收参数
        addr_id = request.POST.get('addr_id')
        pay_method = request.POST.get('pay_method')
        sku_ids = request.POST.get('sku_ids')

        # 校验参数
        if not all([addr_id, pay_method, sku_ids]):
            return JsonResponse({'res': 1, 'errmsg': '参数不完整'})

        # 校验支付方式
        if pay_method not in OrderInfo.PAY_METHODS.keys():
            return JsonResponse({'res': 2, 'errmsg': '非法的支付'})

        # 校验地址
        try:
            addr = Address.objects.get(id=addr_id)
        except Address.DoesNotExist:
            return JsonResponse({'res': 3, 'errmsg': '地址非法'})

        # todo:创建订单核心业务
        # 创建订单记录缺少的参数：order_id  total_count  total_price transit_price
        # 组织参数
        # 订单id ： 10771122181630+用戶id
        order_id = datetime.now().strftime('%Y%m%d%H%M%S') + str(user.id)

        # 运费
        transit_price = 10

        # 总数目和总金额
        total_count = 0
        total_price = 0

        #设置事务保存点
        save_id=transaction.savepoint()
        try:
            # todo:向df_order_info表中添加一条记录
            order = OrderInfo.objects.create(order_id=order_id,
                                             user=user,
                                             addr=addr,
                                             pay_method=pay_method,
                                             total_count=total_count,
                                             total_price=total_price,
                                             transit_price=transit_price)
            # todo:用户的订单中有几个商品，需要向df_order_goods表中加入几条记录
            conn = get_redis_connection('default')
            cart_key = 'cart_%d' % user.id

            #由于获取到的sku_ids的格式是  1，25 这里要进行拆分
            sku_ids = sku_ids.split(',')
            for sku_id in sku_ids:
                    #让乐观锁尝试3次
                for i in range(3):
                    # 获取商品的信息
                    try:
                        #增加悲观锁
                        sku = GoodsSKU.objects.get(id=sku_id)
                    except:
                        # 商品不存在
                        #将回滚到保存点的位置 不对数据保存
                        transaction.savepoint_rollback(save_id)
                        return JsonResponse({'res': 4, 'errmsg': '商品不存在'})
                    print('user:%d stock%d'%(user.id,sku.stock))
                    # import time #休眠一下 测试
                    # time.sleep(10)
                    # 从redis中获取用户所需要购买的商品的数量
                    count = conn.hget(cart_key, sku_id)

                    #todo:判断商品库存
                    #当两个用户同时购买一件商品 前一个用户已经吧商品买完库存不足，下一个用户购买时会抱错
                    if int(count)>sku.stock:
                        transaction.savepoint_rollback(save_id)
                        return JsonResponse({'res':6,'errmsg':'商品库存不足'})
                    # todo:更新商品的库存和销量
                    #保存原始库存
                    orgin_stock=sku.stock
                    new_stock =orgin_stock-int(count)
                    new_sales =sku.sales- int(count)

                    #update df_goods_sku set stock=new_stock,sales=new_sales
                    #where id=sku.id and stock=orgin_stock
                    #返回受影响的行数
                    res=GoodsSKU.objects.filter(id=sku_id,stock=orgin_stock).update(stock=new_stock,sales=new_sales)
                    #更新失败 说明在更新之前确实有人把库存已经修改了
                    if res==0:
                        #当尝试第三次都没成功 -- 下单失败
                        if i==2:
                            #受影响行数为0 事务回滚并报出错误
                            transaction.savepoint_rollback(save_id)
                            return JsonResponse({'res':7,'errmsg':'下单失败'})
                        continue
                    # todo:向df_order_goods表中添加一条记录
                    OrderGoods.objects.create(order=order,
                                              sku=sku,
                                              count=count,
                                              price=sku.price
                                              )

                    # todo:累加计算订单商品的总数目和总价格
                    amount = sku.price * int(count)

                    total_count += int(count)
                    total_price += amount
                    #跳出循环
                    break
            # todo:更新订单信息表中的总数量和总价格
            order.total_count = total_count
            order.total_price = total_price
            order.save()
        except Exception as e:
            transaction.savepoint_rollback(save_id)
            return JsonResponse({'res':7,'errmsg':'下单失败'})

        #提交事务
        transaction.savepoint_commit(save_id)
        # todo:清除用户购物车中对应记录 加*会对列表中的所有数进行删除
        conn.hdel(cart_key, *sku_ids)

        # 返回应答
        return JsonResponse({'res': 5, 'message': '创建成功'})

#订单支付
#ajax post请求
#前端传递的参数：订单id(order_id)
class OrderPayView(View):
    #订单支付
    def post(self,request):
        #用户是否登录
        user=request.user
        if not user.is_authenticated:
            return JsonResponse({'res':0,'errmsg':'用户未登录'})

        #接收参数
        order_id=request.POST.get('order_id')

        #校验参数
        if not order_id:
            return JsonResponse({'res':1,'errmsg':'无效的订单id'})

        try:
            #判断订单id对应 用户对应 支付方式为支付宝 顶戴状态为待支付
            order=OrderInfo.objects.get(order_id=order_id,
                                        user=user,
                                        pay_method=3,
                                        order_status=1)
        except OrderInfo.DoesNotExist:
            return JsonResponse({'res':2,'errmsg':'订单错误'})

        #业务处理：使用python sdk调用支付宝接口
        #初始化

        alipay = AliPay(
            appid='2016101200670794',  # 应用id
            app_notify_url=None,  # 默认回调url
            # app_private_key_string=os.path.join(settings.BASE_DIR, 'apps/fresh_order/app_private_key.pem'),
            app_private_key_string=open('apps/fresh_order/app_private_key.pem').read(),
            # 这里写我们目录下的私钥路径
            # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
            # alipay_public_key_string=os.path.join(settings.BASE_DIR, 'apps/fresh_order/alipay_public_key.pem'),
            alipay_public_key_string=open('apps/fresh_order/alipay_public_key.pem').read(),
            # 这里写我们目录下的公钥路径
            sign_type="RSA2",  # RSA 或者 RSA2 推荐使用RSA2
            debug=True  # 默认False False时调用实际接口 True调用沙箱测试接口
        )

        #调用支付的接口     
        # 电脑网站支付，需要跳转到https://openapi.alipaydev.com/gateway.do? + order_string
        total_pay=order.total_price+order.transit_price #Decial格式 需要str转换
        order_string = alipay.api_alipay_trade_page_pay(
            out_trade_no=order_id, #订单id
            total_amount=str(total_pay),
            subject='海马生鲜%s'%order_id, #标题
            return_url=None,
            notify_url=None  # 可选, 不填则使用默认notify url
        )

        #返回应答
        pay_url='https://openapi.alipaydev.com/gateway.do?'+order_string
        print(pay_url)
        return JsonResponse({'res':3,'pay_url':pay_url})


#检查订单是否成功支付
#ajax post请求
#/order/check
class OrderCheckView(View):
    def post(self,request):
        # 用户是否登录
        user = request.user
        if not user.is_authenticated:
            return JsonResponse({'res': 0, 'errmsg': '用户未登录'})

        # 接收参数
        order_id = request.POST.get('order_id')

        # 校验参数
        if not order_id:
            return JsonResponse({'res': 1, 'errmsg': '无效的订单id'})

        try:
            # 判断订单id对应 用户对应 支付方式为支付宝 顶戴状态为待支付
            order = OrderInfo.objects.get(order_id=order_id,
                                          user=user,
                                          pay_method=3,
                                          order_status=1)
        except OrderInfo.DoesNotExist:
            return JsonResponse({'res': 2, 'errmsg': '订单错误'})

        # 业务处理：使用python sdk调用支付宝接口
        # 初始化
        alipay = AliPay(
            appid='2016101200670794',  # 应用id
            app_notify_url=None,  # 默认回调url
            # app_private_key_string=os.path.join(settings.BASE_DIR, 'apps/fresh_order/app_private_key.pem'),
            app_private_key_string=open('apps/fresh_order/app_private_key.pem').read(),
            # 这里写我们目录下的私钥路径
            # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
            # alipay_public_key_string=os.path.join(settings.BASE_DIR, 'apps/fresh_order/alipay_public_key.pem'),
            alipay_public_key_string=open('apps/fresh_order/alipay_public_key.pem').read(),
            # 这里写我们目录下的公钥路径
            sign_type="RSA2",  # RSA 或者 RSA2 推荐使用RSA2
            debug=True  # 默认False False时调用实际接口 True调用沙箱测试接口
        )

        #调用支付宝查询支付接口
        #传入两个特殊参数 一个是商户订单号 一个是支付宝交易号，我们这里用沙箱进行测试，所以使用订单号
        while True:
            response=alipay.api_alipay_trade_query(order_id)
            """
                    response = {
                      "alipay_trade_query_response": {
                        "trade_no": "2017032121001004070200176844",
                        "code": "10000",
                        "invoice_amount": "20.00",
                        "open_id": "20880072506750308812798160715407",
                        "fund_bill_list": [
                          {
                            "amount": "20.00",
                            "fund_channel": "ALIPAYACCOUNT"
                          }
                        ],
                        "buyer_logon_id": "csq***@sandbox.com",
                        "send_pay_date": "2017-03-21 13:29:17",
                        "receipt_amount": "20.00",
                        "out_trade_no": "out_trade_no15",
                        "buyer_pay_amount": "20.00",
                        "buyer_user_id": "2088102169481075",
                        "msg": "Success",
                        "point_amount": "0.00",
                        "trade_status": "TRADE_SUCCESS",
                        "total_amount": "20.00"
                    }
                    """
            code=response.get('code')
            if code=='10000' and response.get('trade_status') == "TRADE_SUCCESS":
                #支付成功
                #获取支付宝交易号
                trade_no=response.get('trade_no')
                #更新订单状态
                order.trade_no=trade_no
                order.order_status=4 #因为我们没有送货的过程 这里直接改为待评价
                order.save()
                #返回应答
                return JsonResponse({'res':3,'message':'支付成功'})
            elif code=='40004' or (code=='10000' and response.get('trade_status') == "WAIT_BUYER_PAY"):
                #此时用户为正在支付的状态 我们要休眠5秒等待用户支付
                #业务处理失败 可能一会就会成功
                import time
                time.sleep(5)
                continue
            else:
                return JsonResponse({'res':4,'errmsg':'支付失败'})
