import time
from celery import Celery
from fresh_every_day import settings
from django.core.mail import  send_mail


#创建Celery对象
"""
参数：
1. 实例对象，启动后app对应的名字，虽然可以随意指定，但是在项目里面最好写一个导包的路径
2. broker指定你使用的中间人是谁 
使用的哪个redis就用个redis数据库
"""
#从txt文件中找到celery的redis_ip
with open('使用前修改为自己的redis_ip.txt','r') as f :
    all_content=f.readlines()
app=Celery('celery_tasks.tasks',broker=all_content[0].replace('celery_ip:',''))

@app.task()
def send_register_active_email(to_email, username, token):
    '''发送激活邮件'''
    # 组织邮件信息
    subject = '天天生鲜欢迎信息'
    message = ''
    sender = settings.EMAIL_FROM
    receiver = [to_email,]
    html_message = '<h1>%s, 欢迎您成为天天生鲜注册会员</h1>请点击下面链接激活您的账户<br/><a href="http://127.0.0.1:8000/user/active/%s">http://127.0.0.1:8000/user/active/%s</a>' % (username, token, token)

    send_mail(subject, message, sender, receiver, html_message=html_message)
    time.sleep(5)

import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fresh_every_day.settings')
django.setup()

from django.template import loader,RequestContext
from fresh_goods.models import * #要写在django.setup()的下方 否则会抱错未找到
@app.task()
#首页页面静态化--celery任务函数
def generate_static_index_html():
#     #产生首页静态页面
    # 获取商品首页信息
    types = GoodsType.objects.all()

    # 获取首页轮播图商品信息
    goods_banner = IndexGoodsBanner.objects.all().order_by('index')  # 0,1,2 默认升序

    # 获取首页促销活动信息
    promotion_banners = IndexPromotionBanner.objects.all().order_by('index')  # 0,1,2 默认升序

    # 获取首页分类商品展示信息
    for type in types:  # GoodTypes
        # 获取type种类首页分类商品的图片展示信息
        image_banner = IndexTypeGoodsBanner.objects.filter(type=type, display_type=1).order_by('index')  # 0,1,2 默认升序
        # 获取type种类首页分类商品的文字展示信息
        title_banners = IndexTypeGoodsBanner.objects.filter(type=type, display_type=0).order_by('index')  # 0,1,2 默认升序
        # 动态给type增加属性，分别保存首页分类商品的图片展示信息和文字展示信息
        type.image_banners = image_banner
        type.title_banner = title_banners
    context={
            'types':types,
            'goods_banner':goods_banner,
            'promotion_banners':promotion_banners
            }
    #使用模版
    # 1.加载模版文件,返回模版对象
    temp=loader.get_template('static_index.html')
    # 2.定义模版上下文--可以省略
    # context=RequestContext(request,context)
    # 3.模版渲染
    static_index_html=temp.render(context)
    #生成首页对应静态文件
    save_path=os.path.join(settings.BASE_DIR,'static/index.html')
    with open(save_path,'w') as f:
        f.write(static_index_html)