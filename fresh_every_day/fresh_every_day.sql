/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : fresh_every_day

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-11-08 10:14:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO `auth_permission` VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO `auth_permission` VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO `auth_permission` VALUES ('4', 'Can view log entry', '1', 'view_logentry');
INSERT INTO `auth_permission` VALUES ('5', 'Can add permission', '2', 'add_permission');
INSERT INTO `auth_permission` VALUES ('6', 'Can change permission', '2', 'change_permission');
INSERT INTO `auth_permission` VALUES ('7', 'Can delete permission', '2', 'delete_permission');
INSERT INTO `auth_permission` VALUES ('8', 'Can view permission', '2', 'view_permission');
INSERT INTO `auth_permission` VALUES ('9', 'Can add group', '3', 'add_group');
INSERT INTO `auth_permission` VALUES ('10', 'Can change group', '3', 'change_group');
INSERT INTO `auth_permission` VALUES ('11', 'Can delete group', '3', 'delete_group');
INSERT INTO `auth_permission` VALUES ('12', 'Can view group', '3', 'view_group');
INSERT INTO `auth_permission` VALUES ('13', 'Can add content type', '4', 'add_contenttype');
INSERT INTO `auth_permission` VALUES ('14', 'Can change content type', '4', 'change_contenttype');
INSERT INTO `auth_permission` VALUES ('15', 'Can delete content type', '4', 'delete_contenttype');
INSERT INTO `auth_permission` VALUES ('16', 'Can view content type', '4', 'view_contenttype');
INSERT INTO `auth_permission` VALUES ('17', 'Can add session', '5', 'add_session');
INSERT INTO `auth_permission` VALUES ('18', 'Can change session', '5', 'change_session');
INSERT INTO `auth_permission` VALUES ('19', 'Can delete session', '5', 'delete_session');
INSERT INTO `auth_permission` VALUES ('20', 'Can view session', '5', 'view_session');
INSERT INTO `auth_permission` VALUES ('21', 'Can add 用户', '6', 'add_user');
INSERT INTO `auth_permission` VALUES ('22', 'Can change 用户', '6', 'change_user');
INSERT INTO `auth_permission` VALUES ('23', 'Can delete 用户', '6', 'delete_user');
INSERT INTO `auth_permission` VALUES ('24', 'Can view 用户', '6', 'view_user');
INSERT INTO `auth_permission` VALUES ('25', 'Can add captcha store', '7', 'add_captchastore');
INSERT INTO `auth_permission` VALUES ('26', 'Can change captcha store', '7', 'change_captchastore');
INSERT INTO `auth_permission` VALUES ('27', 'Can delete captcha store', '7', 'delete_captchastore');
INSERT INTO `auth_permission` VALUES ('28', 'Can view captcha store', '7', 'view_captchastore');
INSERT INTO `auth_permission` VALUES ('29', 'Can add 地址', '8', 'add_address');
INSERT INTO `auth_permission` VALUES ('30', 'Can change 地址', '8', 'change_address');
INSERT INTO `auth_permission` VALUES ('31', 'Can delete 地址', '8', 'delete_address');
INSERT INTO `auth_permission` VALUES ('32', 'Can view 地址', '8', 'view_address');
INSERT INTO `auth_permission` VALUES ('33', 'Can add 主页促销活动', '9', 'add_indexpromotionbanner');
INSERT INTO `auth_permission` VALUES ('34', 'Can change 主页促销活动', '9', 'change_indexpromotionbanner');
INSERT INTO `auth_permission` VALUES ('35', 'Can delete 主页促销活动', '9', 'delete_indexpromotionbanner');
INSERT INTO `auth_permission` VALUES ('36', 'Can view 主页促销活动', '9', 'view_indexpromotionbanner');
INSERT INTO `auth_permission` VALUES ('37', 'Can add 主页分类展示商品', '10', 'add_indextypegoodsbanner');
INSERT INTO `auth_permission` VALUES ('38', 'Can change 主页分类展示商品', '10', 'change_indextypegoodsbanner');
INSERT INTO `auth_permission` VALUES ('39', 'Can delete 主页分类展示商品', '10', 'delete_indextypegoodsbanner');
INSERT INTO `auth_permission` VALUES ('40', 'Can view 主页分类展示商品', '10', 'view_indextypegoodsbanner');
INSERT INTO `auth_permission` VALUES ('41', 'Can add 商品图片', '11', 'add_goodsimage');
INSERT INTO `auth_permission` VALUES ('42', 'Can change 商品图片', '11', 'change_goodsimage');
INSERT INTO `auth_permission` VALUES ('43', 'Can delete 商品图片', '11', 'delete_goodsimage');
INSERT INTO `auth_permission` VALUES ('44', 'Can view 商品图片', '11', 'view_goodsimage');
INSERT INTO `auth_permission` VALUES ('45', 'Can add 商品SPU', '12', 'add_goods');
INSERT INTO `auth_permission` VALUES ('46', 'Can change 商品SPU', '12', 'change_goods');
INSERT INTO `auth_permission` VALUES ('47', 'Can delete 商品SPU', '12', 'delete_goods');
INSERT INTO `auth_permission` VALUES ('48', 'Can view 商品SPU', '12', 'view_goods');
INSERT INTO `auth_permission` VALUES ('49', 'Can add 商品', '13', 'add_goodssku');
INSERT INTO `auth_permission` VALUES ('50', 'Can change 商品', '13', 'change_goodssku');
INSERT INTO `auth_permission` VALUES ('51', 'Can delete 商品', '13', 'delete_goodssku');
INSERT INTO `auth_permission` VALUES ('52', 'Can view 商品', '13', 'view_goodssku');
INSERT INTO `auth_permission` VALUES ('53', 'Can add 首页轮播商品', '14', 'add_indexgoodsbanner');
INSERT INTO `auth_permission` VALUES ('54', 'Can change 首页轮播商品', '14', 'change_indexgoodsbanner');
INSERT INTO `auth_permission` VALUES ('55', 'Can delete 首页轮播商品', '14', 'delete_indexgoodsbanner');
INSERT INTO `auth_permission` VALUES ('56', 'Can view 首页轮播商品', '14', 'view_indexgoodsbanner');
INSERT INTO `auth_permission` VALUES ('57', 'Can add 商品种类', '15', 'add_goodstype');
INSERT INTO `auth_permission` VALUES ('58', 'Can change 商品种类', '15', 'change_goodstype');
INSERT INTO `auth_permission` VALUES ('59', 'Can delete 商品种类', '15', 'delete_goodstype');
INSERT INTO `auth_permission` VALUES ('60', 'Can view 商品种类', '15', 'view_goodstype');
INSERT INTO `auth_permission` VALUES ('61', 'Can add 订单', '16', 'add_orderinfo');
INSERT INTO `auth_permission` VALUES ('62', 'Can change 订单', '16', 'change_orderinfo');
INSERT INTO `auth_permission` VALUES ('63', 'Can delete 订单', '16', 'delete_orderinfo');
INSERT INTO `auth_permission` VALUES ('64', 'Can view 订单', '16', 'view_orderinfo');
INSERT INTO `auth_permission` VALUES ('65', 'Can add 订单商品', '17', 'add_ordergoods');
INSERT INTO `auth_permission` VALUES ('66', 'Can change 订单商品', '17', 'change_ordergoods');
INSERT INTO `auth_permission` VALUES ('67', 'Can delete 订单商品', '17', 'delete_ordergoods');
INSERT INTO `auth_permission` VALUES ('68', 'Can view 订单商品', '17', 'view_ordergoods');

-- ----------------------------
-- Table structure for captcha_captchastore
-- ----------------------------
DROP TABLE IF EXISTS `captcha_captchastore`;
CREATE TABLE `captcha_captchastore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge` varchar(32) NOT NULL,
  `response` varchar(32) NOT NULL,
  `hashkey` varchar(40) NOT NULL,
  `expiration` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashkey` (`hashkey`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of captcha_captchastore
-- ----------------------------
INSERT INTO `captcha_captchastore` VALUES ('247', '7*5=', '35', '9cca884e761093c0790c2c4ed3221d725d3241e4', '2019-10-18 00:19:28.436570');
INSERT INTO `captcha_captchastore` VALUES ('248', '9*7=', '63', 'ce16203daded891203bcfaba2cf8983ae70c5951', '2019-10-18 00:19:28.485453');
INSERT INTO `captcha_captchastore` VALUES ('250', '10-3=', '7', '3fd74b056152f0b17893e9d9fa09867e01efbe5a', '2019-10-18 00:19:30.231955');
INSERT INTO `captcha_captchastore` VALUES ('251', '1*6=', '6', '8d6d0daa991850fdfd7ce492bc79f93df5c90cdf', '2019-10-18 00:19:39.940829');

-- ----------------------------
-- Table structure for df_address
-- ----------------------------
DROP TABLE IF EXISTS `df_address`;
CREATE TABLE `df_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `receiver` varchar(20) NOT NULL,
  `addr` varchar(256) NOT NULL,
  `zip_code` varchar(6) DEFAULT NULL,
  `phone` varchar(11) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_address_user_id_5e6a5c8a_fk_df_user_id` (`user_id`),
  CONSTRAINT `df_address_user_id_5e6a5c8a_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_address
-- ----------------------------
INSERT INTO `df_address` VALUES ('3', '2019-09-12 07:49:55.351233', '2019-09-12 07:49:55.351233', '0', '翼德', '北京市房山区', '100000', '13333333333', '1', '38');
INSERT INTO `df_address` VALUES ('4', '2019-09-12 07:50:11.101493', '2019-09-12 07:50:11.101493', '0', '翼德', '北京市房山区', '100000', '13333333333', '0', '38');

-- ----------------------------
-- Table structure for df_goods
-- ----------------------------
DROP TABLE IF EXISTS `df_goods`;
CREATE TABLE `df_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `detail` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_goods
-- ----------------------------
INSERT INTO `df_goods` VALUES ('1', '2019-09-16 12:20:17.396854', '2019-09-16 12:20:17.396854', '0', '18636350081', '进口美国草莓', '外表好看，口感润滑');
INSERT INTO `df_goods` VALUES ('2', '2019-09-16 12:22:38.594532', '2019-09-16 12:22:38.594532', '0', '18636350081', '进口葡萄', '口感润滑，好吃');
INSERT INTO `df_goods` VALUES ('3', '2019-09-16 12:23:46.464196', '2019-09-16 12:23:46.465183', '0', '18636350081', '新鲜柠檬', '酸甜可口，刺激味蕾');
INSERT INTO `df_goods` VALUES ('4', '2019-09-16 12:24:45.154321', '2019-09-16 12:24:45.154321', '0', '18636350081', '进口奇异果', '美味好吃，物美价廉');
INSERT INTO `df_goods` VALUES ('5', '2019-09-16 12:27:04.907274', '2019-09-16 12:27:14.467827', '0', '18636350081', '青岛野生海捕大青虾', '口感润滑，食欲大增');
INSERT INTO `df_goods` VALUES ('6', '2019-09-16 12:28:01.043436', '2019-09-16 12:28:08.487859', '0', '18636350081', '进口扇贝', '');
INSERT INTO `df_goods` VALUES ('7', '2019-09-16 12:28:51.046709', '2019-09-16 12:28:51.046709', '0', '18636350081', '冷冻秋刀鱼', '');
INSERT INTO `df_goods` VALUES ('8', '2019-09-16 12:29:28.815641', '2019-09-16 12:29:28.815641', '0', '18636350081', '基围虾', '');
INSERT INTO `df_goods` VALUES ('9', '2019-09-16 12:30:19.889411', '2019-09-16 12:30:19.889411', '0', '18636350081', '维多利亚葡萄', '');
INSERT INTO `df_goods` VALUES ('10', '2019-09-17 00:03:04.566163', '2019-09-17 00:03:04.566163', '0', '18636350081', '鲜芒', '');
INSERT INTO `df_goods` VALUES ('11', '2019-09-17 00:05:18.036715', '2019-09-17 00:05:18.036715', '0', '18636350081', '加州提子', '');
INSERT INTO `df_goods` VALUES ('12', '2019-09-17 00:15:22.973712', '2019-09-17 00:15:22.973712', '0', '18636350081', '亚马逊牛油果', '');
INSERT INTO `df_goods` VALUES ('13', '2019-09-17 00:17:03.029426', '2019-09-17 00:17:03.030422', '0', '18636350081', '河虾', '');

-- ----------------------------
-- Table structure for df_goods_image
-- ----------------------------
DROP TABLE IF EXISTS `df_goods_image`;
CREATE TABLE `df_goods_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `sku_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_goods_image_sku_id_f8dc96ea_fk_df_goods_sku_id` (`sku_id`),
  CONSTRAINT `df_goods_image_sku_id_f8dc96ea_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_goods_image
-- ----------------------------

-- ----------------------------
-- Table structure for df_goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `df_goods_sku`;
CREATE TABLE `df_goods_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `desc` varchar(256) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `unite` varchar(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `sales` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_goods_sku_goods_id_31622280_fk_df_goods_id` (`goods_id`),
  KEY `df_goods_sku_type_id_576de3b4_fk_df_goods_type_id` (`type_id`),
  CONSTRAINT `df_goods_sku_goods_id_31622280_fk_df_goods_id` FOREIGN KEY (`goods_id`) REFERENCES `df_goods` (`id`),
  CONSTRAINT `df_goods_sku_type_id_576de3b4_fk_df_goods_type_id` FOREIGN KEY (`type_id`) REFERENCES `df_goods_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_goods_sku
-- ----------------------------
INSERT INTO `df_goods_sku` VALUES ('1', '2019-09-16 12:21:58.443443', '2019-09-16 12:21:58.443443', '0', '18636350081', '草莓', '香甜可口的草莓', '30.00', '元', 'goods/goods003.jpg', '50', '0', '1', '1', '1');
INSERT INTO `df_goods_sku` VALUES ('2', '2019-09-16 12:23:03.803320', '2019-09-16 12:23:03.803320', '0', '18636350081', '葡萄', '香甜可口的葡萄', '5.50', '元', 'goods/goods002.jpg', '37', '-13', '1', '2', '1');
INSERT INTO `df_goods_sku` VALUES ('3', '2019-09-16 12:24:16.645257', '2019-09-16 12:24:16.645257', '0', '18636350081', '柠檬', '香甜可口的柠檬', '3.90', '元', 'goods/goods001.jpg', '45', '-5', '1', '3', '1');
INSERT INTO `df_goods_sku` VALUES ('4', '2019-09-16 12:25:12.215684', '2019-09-16 12:25:12.215684', '0', '18636350081', '奇异果', '香甜可口的奇异果', '25.80', '元', 'goods/goods012.jpg', '50', '0', '1', '4', '1');
INSERT INTO `df_goods_sku` VALUES ('5', '2019-09-16 12:27:45.370802', '2019-09-16 12:27:45.370802', '0', '18636350081', '青岛野生海捕大青虾', '美味好吃的青岛野生海捕大青虾', '48.00', '元', 'goods/goods018.jpg', '47', '-2', '1', '5', '2');
INSERT INTO `df_goods_sku` VALUES ('6', '2019-09-16 12:28:35.857967', '2019-09-16 12:28:35.857967', '0', '18636350081', '扇贝', '美味好吃的扇贝', '46.00', '元', 'goods/goods019.jpg', '49', '-1', '1', '6', '2');
INSERT INTO `df_goods_sku` VALUES ('7', '2019-09-16 12:29:10.733103', '2019-09-16 12:29:10.733103', '0', '18636350081', '冷冻秋刀鱼', '美味好吃的冷冻秋刀鱼', '19.00', '元', 'goods/goods020.jpg', '49', '-1', '1', '7', '2');
INSERT INTO `df_goods_sku` VALUES ('8', '2019-09-16 12:29:49.458134', '2019-09-16 12:29:49.458134', '0', '18636350081', '基围虾', '美味好吃的基围虾', '25.00', '元', 'goods/goods021.jpg', '50', '0', '1', '8', '2');
INSERT INTO `df_goods_sku` VALUES ('9', '2019-09-16 12:31:11.699998', '2019-09-16 12:31:11.699998', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods.jpg', '50', '0', '1', '9', '3');
INSERT INTO `df_goods_sku` VALUES ('10', '2019-09-16 12:31:35.638009', '2019-09-16 12:31:35.638009', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_KHv7Jxa.jpg', '50', '0', '1', '9', '3');
INSERT INTO `df_goods_sku` VALUES ('11', '2019-09-16 12:31:54.786645', '2019-09-16 12:31:54.786645', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_keolpwr.jpg', '50', '0', '1', '9', '3');
INSERT INTO `df_goods_sku` VALUES ('12', '2019-09-16 12:32:15.334167', '2019-09-16 12:32:15.334167', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_XV1UBGs.jpg', '50', '0', '1', '9', '3');
INSERT INTO `df_goods_sku` VALUES ('13', '2019-09-16 12:32:41.603149', '2019-09-16 12:32:50.941965', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_bggip66.jpg', '50', '0', '1', '9', '4');
INSERT INTO `df_goods_sku` VALUES ('14', '2019-09-16 12:33:09.338759', '2019-09-16 12:33:09.338759', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_GCV4Saa.jpg', '50', '0', '1', '9', '4');
INSERT INTO `df_goods_sku` VALUES ('15', '2019-09-16 12:33:28.869507', '2019-09-16 12:33:28.869507', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_4XwfrVx.jpg', '50', '0', '1', '9', '4');
INSERT INTO `df_goods_sku` VALUES ('16', '2019-09-16 12:33:53.696717', '2019-09-16 12:33:53.696717', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_cHOL0zn.jpg', '50', '0', '1', '9', '4');
INSERT INTO `df_goods_sku` VALUES ('17', '2019-09-16 12:34:12.202078', '2019-09-16 12:34:12.202078', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_FNHZp0J.jpg', '50', '0', '1', '9', '5');
INSERT INTO `df_goods_sku` VALUES ('18', '2019-09-16 12:34:29.184962', '2019-09-16 12:34:29.184962', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_Ec9Z5vz.jpg', '50', '0', '1', '9', '5');
INSERT INTO `df_goods_sku` VALUES ('19', '2019-09-16 12:34:47.585716', '2019-09-16 12:34:47.585716', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_rpHGQyP.jpg', '50', '0', '1', '9', '5');
INSERT INTO `df_goods_sku` VALUES ('20', '2019-09-16 12:35:05.255281', '2019-09-16 12:35:05.255281', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_TEbIdrS.jpg', '50', '0', '1', '9', '5');
INSERT INTO `df_goods_sku` VALUES ('21', '2019-09-16 12:35:24.472982', '2019-09-16 12:35:24.472982', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_OobLzC9.jpg', '50', '0', '1', '9', '6');
INSERT INTO `df_goods_sku` VALUES ('22', '2019-09-16 12:35:42.027777', '2019-09-16 12:35:42.027777', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_46V2yOY.jpg', '50', '0', '1', '9', '6');
INSERT INTO `df_goods_sku` VALUES ('23', '2019-09-16 12:35:59.570315', '2019-09-16 12:35:59.570315', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_2u0ynLi.jpg', '50', '0', '1', '9', '6');
INSERT INTO `df_goods_sku` VALUES ('24', '2019-09-16 12:36:18.325135', '2019-09-16 12:36:18.325135', '0', '18636350081', '维多利亚葡萄', '香甜可口的维多利亚葡萄', '38.00', '元', 'goods/goods_xlhy7I0.jpg', '50', '0', '1', '9', '6');
INSERT INTO `df_goods_sku` VALUES ('25', '2019-09-17 00:03:50.227551', '2019-09-17 00:03:50.227551', '0', '18636350081', '鲜芒', '香甜可口的鲜芒', '25.00', '元', 'goods/goods016.jpg', '50', '0', '1', '10', '1');
INSERT INTO `df_goods_sku` VALUES ('26', '2019-09-17 00:05:39.527509', '2019-09-17 00:05:39.527509', '0', '18636350081', '加州提子', '香甜可口的加州提子', '35.00', '元', 'goods/goods008.jpg', '50', '0', '1', '11', '1');
INSERT INTO `df_goods_sku` VALUES ('27', '2019-09-17 00:15:47.268948', '2019-09-17 00:15:47.268948', '0', '18636350081', '亚马逊牛油果', '香甜可口的亚马逊牛油果', '46.00', '元', 'goods/goods007.jpg', '50', '0', '1', '12', '1');
INSERT INTO `df_goods_sku` VALUES ('28', '2019-09-17 00:17:31.827742', '2019-09-17 00:17:31.827742', '0', '18636350081', '河虾', '美味好吃的河虾', '35.00', '元', 'goods/goods018_wXsBMsH.jpg', '50', '0', '1', '13', '2');

-- ----------------------------
-- Table structure for df_goods_type
-- ----------------------------
DROP TABLE IF EXISTS `df_goods_type`;
CREATE TABLE `df_goods_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `logo` varchar(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_goods_type
-- ----------------------------
INSERT INTO `df_goods_type` VALUES ('1', '2019-09-16 12:12:54.410509', '2019-09-17 00:11:54.300225', '0', '18636350081', '新鲜水果', 'fruit', 'type/banner01.jpg');
INSERT INTO `df_goods_type` VALUES ('2', '2019-09-16 12:13:36.395725', '2019-09-17 00:10:57.986378', '0', '18636350081', '海鲜水产', 'seafood', 'type/banner02.jpg');
INSERT INTO `df_goods_type` VALUES ('3', '2019-09-16 12:13:57.644546', '2019-09-17 00:11:03.660480', '0', '18636350081', '猪羊牛肉', 'meet', 'type/banner03.jpg');
INSERT INTO `df_goods_type` VALUES ('4', '2019-09-16 12:14:19.766792', '2019-09-17 00:11:07.756030', '0', '18636350081', '禽类蛋品', 'egg', 'type/banner04.jpg');
INSERT INTO `df_goods_type` VALUES ('5', '2019-09-16 12:14:40.960940', '2019-09-17 00:11:21.460261', '0', '18636350081', '新鲜蔬菜', 'vegetables', 'type/banner05.jpg');
INSERT INTO `df_goods_type` VALUES ('6', '2019-09-16 12:15:12.356183', '2019-09-17 00:11:33.760411', '0', '18636350081', '速冻食品', 'ice', 'type/banner06.jpg');

-- ----------------------------
-- Table structure for df_index_banner
-- ----------------------------
DROP TABLE IF EXISTS `df_index_banner`;
CREATE TABLE `df_index_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `index` smallint(6) NOT NULL,
  `sku_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_index_banner_sku_id_57f2798e_fk_df_goods_sku_id` (`sku_id`),
  CONSTRAINT `df_index_banner_sku_id_57f2798e_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_index_banner
-- ----------------------------
INSERT INTO `df_index_banner` VALUES ('1', '2019-09-16 12:48:03.302272', '2019-09-16 12:48:03.302272', '0', '18636350081', 'banner/slide.jpg', '0', '2');
INSERT INTO `df_index_banner` VALUES ('2', '2019-09-16 12:48:30.017474', '2019-09-16 12:48:30.017474', '0', '18636350081', 'banner/slide02.jpg', '1', '3');
INSERT INTO `df_index_banner` VALUES ('3', '2019-09-16 12:49:04.481049', '2019-09-16 12:49:04.481049', '0', '18636350081', 'banner/slide03.jpg', '2', '10');
INSERT INTO `df_index_banner` VALUES ('4', '2019-09-16 12:49:28.499548', '2019-09-16 12:49:28.499548', '0', '18636350081', 'banner/slide04.jpg', '3', '5');

-- ----------------------------
-- Table structure for df_index_promotion
-- ----------------------------
DROP TABLE IF EXISTS `df_index_promotion`;
CREATE TABLE `df_index_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `url` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `index` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_index_promotion
-- ----------------------------
INSERT INTO `df_index_promotion` VALUES ('1', '2019-09-16 12:08:54.385134', '2019-09-16 12:08:54.385134', '0', '18636350081', '吃货暑假趴', 'http://www.aichico.online', 'banner/adv01.jpg', '0');
INSERT INTO `df_index_promotion` VALUES ('2', '2019-09-16 12:09:36.764137', '2019-09-16 12:09:36.764137', '0', '18636350081', '感夏尝鲜季', 'http://www.aichico.online', 'banner/adv02.jpg', '1');

-- ----------------------------
-- Table structure for df_index_type_goods
-- ----------------------------
DROP TABLE IF EXISTS `df_index_type_goods`;
CREATE TABLE `df_index_type_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `display_type` smallint(6) NOT NULL,
  `index` smallint(6) NOT NULL,
  `sku_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_index_type_goods_sku_id_0a8a17db_fk_df_goods_sku_id` (`sku_id`),
  KEY `df_index_type_goods_type_id_35192ffd_fk_df_goods_type_id` (`type_id`),
  CONSTRAINT `df_index_type_goods_sku_id_0a8a17db_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`),
  CONSTRAINT `df_index_type_goods_type_id_35192ffd_fk_df_goods_type_id` FOREIGN KEY (`type_id`) REFERENCES `df_goods_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_index_type_goods
-- ----------------------------
INSERT INTO `df_index_type_goods` VALUES ('5', '2019-09-16 12:40:51.052458', '2019-09-16 12:40:51.052458', '0', '18636350081', '1', '0', '1', '1');
INSERT INTO `df_index_type_goods` VALUES ('6', '2019-09-16 12:40:58.890038', '2019-09-16 12:40:58.890038', '0', '18636350081', '1', '1', '2', '1');
INSERT INTO `df_index_type_goods` VALUES ('7', '2019-09-16 12:41:05.419489', '2019-09-16 12:41:05.419489', '0', '18636350081', '1', '2', '3', '1');
INSERT INTO `df_index_type_goods` VALUES ('8', '2019-09-16 12:41:14.437486', '2019-09-16 12:41:14.437486', '0', '18636350081', '1', '3', '4', '1');
INSERT INTO `df_index_type_goods` VALUES ('9', '2019-09-16 12:41:29.316078', '2019-09-16 12:41:29.316078', '0', '18636350081', '1', '0', '5', '2');
INSERT INTO `df_index_type_goods` VALUES ('10', '2019-09-16 12:41:37.932176', '2019-09-16 12:41:37.932176', '0', '18636350081', '1', '1', '6', '2');
INSERT INTO `df_index_type_goods` VALUES ('11', '2019-09-16 12:41:46.378635', '2019-09-16 12:41:46.378635', '0', '18636350081', '1', '2', '7', '2');
INSERT INTO `df_index_type_goods` VALUES ('12', '2019-09-16 12:41:52.696101', '2019-09-16 12:41:52.696101', '0', '18636350081', '1', '3', '8', '2');
INSERT INTO `df_index_type_goods` VALUES ('13', '2019-09-16 12:42:03.866614', '2019-09-16 12:42:03.867611', '0', '18636350081', '1', '0', '9', '3');
INSERT INTO `df_index_type_goods` VALUES ('14', '2019-09-16 12:42:10.412763', '2019-09-16 12:42:10.412763', '0', '18636350081', '1', '1', '10', '3');
INSERT INTO `df_index_type_goods` VALUES ('15', '2019-09-16 12:42:19.281459', '2019-09-16 12:42:19.281459', '0', '18636350081', '1', '2', '11', '3');
INSERT INTO `df_index_type_goods` VALUES ('16', '2019-09-16 12:42:51.397154', '2019-09-16 12:42:51.397154', '0', '18636350081', '1', '3', '12', '3');
INSERT INTO `df_index_type_goods` VALUES ('17', '2019-09-16 12:43:00.145530', '2019-09-16 12:43:00.145530', '0', '18636350081', '1', '0', '13', '4');
INSERT INTO `df_index_type_goods` VALUES ('18', '2019-09-16 12:43:09.701449', '2019-09-16 12:43:09.701449', '0', '18636350081', '1', '1', '14', '4');
INSERT INTO `df_index_type_goods` VALUES ('19', '2019-09-16 12:43:16.594047', '2019-09-16 12:43:16.594047', '0', '18636350081', '1', '2', '15', '4');
INSERT INTO `df_index_type_goods` VALUES ('20', '2019-09-16 12:43:24.276276', '2019-09-16 12:43:24.276276', '0', '18636350081', '1', '3', '16', '4');
INSERT INTO `df_index_type_goods` VALUES ('21', '2019-09-16 12:43:43.446265', '2019-09-16 12:43:43.446265', '0', '18636350081', '1', '0', '17', '5');
INSERT INTO `df_index_type_goods` VALUES ('22', '2019-09-16 12:43:49.807986', '2019-09-16 12:43:49.807986', '0', '18636350081', '1', '1', '18', '5');
INSERT INTO `df_index_type_goods` VALUES ('23', '2019-09-16 12:43:57.274660', '2019-09-16 12:43:57.274660', '0', '18636350081', '1', '2', '19', '5');
INSERT INTO `df_index_type_goods` VALUES ('24', '2019-09-16 12:44:04.429546', '2019-09-16 12:44:04.429546', '0', '18636350081', '1', '3', '20', '5');
INSERT INTO `df_index_type_goods` VALUES ('25', '2019-09-16 12:44:27.076382', '2019-09-16 12:44:27.076382', '0', '18636350081', '1', '0', '21', '6');
INSERT INTO `df_index_type_goods` VALUES ('26', '2019-09-16 12:44:34.303122', '2019-09-16 12:44:34.303122', '0', '18636350081', '1', '1', '22', '6');
INSERT INTO `df_index_type_goods` VALUES ('27', '2019-09-16 12:44:41.431461', '2019-09-16 12:44:41.431461', '0', '18636350081', '1', '2', '23', '6');
INSERT INTO `df_index_type_goods` VALUES ('28', '2019-09-16 12:44:49.612964', '2019-09-16 12:44:49.612964', '0', '18636350081', '1', '3', '24', '6');
INSERT INTO `df_index_type_goods` VALUES ('29', '2019-09-17 00:03:55.871090', '2019-09-17 00:03:55.871090', '0', '18636350081', '0', '0', '25', '1');
INSERT INTO `df_index_type_goods` VALUES ('30', '2019-09-17 00:05:50.727857', '2019-09-17 00:05:50.727857', '0', '18636350081', '0', '1', '26', '1');
INSERT INTO `df_index_type_goods` VALUES ('32', '2019-09-17 00:16:05.503668', '2019-09-17 00:16:14.134605', '0', '18636350081', '0', '2', '27', '1');
INSERT INTO `df_index_type_goods` VALUES ('33', '2019-09-17 00:17:36.064170', '2019-09-17 00:17:36.064170', '0', '18636350081', '0', '0', '28', '2');
INSERT INTO `df_index_type_goods` VALUES ('34', '2019-09-17 00:17:50.190674', '2019-09-17 00:17:50.190674', '0', '18636350081', '0', '1', '6', '2');
INSERT INTO `df_index_type_goods` VALUES ('35', '2019-09-17 00:18:38.190154', '2019-09-17 00:18:38.190154', '0', '18636350081', '0', '0', '25', '3');
INSERT INTO `df_index_type_goods` VALUES ('36', '2019-09-17 00:18:45.972127', '2019-09-17 00:18:45.972127', '0', '18636350081', '0', '1', '26', '3');
INSERT INTO `df_index_type_goods` VALUES ('37', '2019-09-17 00:18:53.270644', '2019-09-17 00:18:53.270644', '0', '18636350081', '0', '2', '27', '3');
INSERT INTO `df_index_type_goods` VALUES ('38', '2019-09-17 00:19:01.419400', '2019-09-17 00:19:01.419400', '0', '18636350081', '0', '0', '25', '4');
INSERT INTO `df_index_type_goods` VALUES ('39', '2019-09-17 00:19:08.856998', '2019-09-17 00:19:08.856998', '0', '18636350081', '0', '1', '26', '4');
INSERT INTO `df_index_type_goods` VALUES ('40', '2019-09-17 00:19:20.677019', '2019-09-17 00:19:20.677019', '0', '18636350081', '0', '2', '27', '4');
INSERT INTO `df_index_type_goods` VALUES ('41', '2019-09-17 00:19:27.493096', '2019-09-17 00:19:27.493096', '0', '18636350081', '0', '0', '25', '5');
INSERT INTO `df_index_type_goods` VALUES ('42', '2019-09-17 00:19:35.969550', '2019-09-17 00:19:35.969550', '0', '18636350081', '0', '1', '26', '5');
INSERT INTO `df_index_type_goods` VALUES ('43', '2019-09-17 00:19:42.505170', '2019-09-17 00:19:42.505170', '0', '18636350081', '0', '2', '27', '5');
INSERT INTO `df_index_type_goods` VALUES ('44', '2019-09-17 00:19:48.980798', '2019-09-17 00:19:48.980798', '0', '18636350081', '0', '0', '25', '6');
INSERT INTO `df_index_type_goods` VALUES ('45', '2019-09-17 00:19:57.930740', '2019-09-17 00:19:57.930740', '0', '18636350081', '0', '1', '26', '6');
INSERT INTO `df_index_type_goods` VALUES ('46', '2019-09-17 00:20:04.848224', '2019-09-17 00:20:04.848224', '0', '18636350081', '0', '2', '27', '6');

-- ----------------------------
-- Table structure for df_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `df_order_goods`;
CREATE TABLE `df_order_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `count` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `comment` varchar(256) NOT NULL,
  `order_id` varchar(128) NOT NULL,
  `sku_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `df_order_goods_order_id_6958ee23_fk_df_order_info_order_id` (`order_id`),
  KEY `df_order_goods_sku_id_b7d6e04e_fk_df_goods_sku_id` (`sku_id`),
  CONSTRAINT `df_order_goods_order_id_6958ee23_fk_df_order_info_order_id` FOREIGN KEY (`order_id`) REFERENCES `df_order_info` (`order_id`),
  CONSTRAINT `df_order_goods_sku_id_b7d6e04e_fk_df_goods_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `df_goods_sku` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_order_goods
-- ----------------------------
INSERT INTO `df_order_goods` VALUES ('9', '2019-09-26 07:02:13.563997', '2019-09-26 07:02:13.563997', '0', '', '5', '5.50', '', '2019092615021338', '2');
INSERT INTO `df_order_goods` VALUES ('10', '2019-10-18 00:25:42.402536', '2019-10-18 00:25:42.402536', '0', '', '1', '5.50', '', '2019101808254238', '2');

-- ----------------------------
-- Table structure for df_order_info
-- ----------------------------
DROP TABLE IF EXISTS `df_order_info`;
CREATE TABLE `df_order_info` (
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `order_id` varchar(128) NOT NULL,
  `pay_method` smallint(6) NOT NULL,
  `total_count` int(11) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `transit_price` decimal(10,2) NOT NULL,
  `order_status` smallint(6) NOT NULL,
  `trade_no` varchar(128) NOT NULL,
  `addr_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `df_order_info_addr_id_70c3726e_fk_df_address_id` (`addr_id`),
  KEY `df_order_info_user_id_ac1e5bf5_fk_df_user_id` (`user_id`),
  CONSTRAINT `df_order_info_addr_id_70c3726e_fk_df_address_id` FOREIGN KEY (`addr_id`) REFERENCES `df_address` (`id`),
  CONSTRAINT `df_order_info_user_id_ac1e5bf5_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_order_info
-- ----------------------------
INSERT INTO `df_order_info` VALUES ('2019-09-26 07:02:13.540096', '2019-10-08 07:54:07.505747', '0', '', '2019092615021338', '3', '5', '27.50', '10.00', '4', '2019100822001419481000048319', '4', '38');
INSERT INTO `df_order_info` VALUES ('2019-10-18 00:25:42.376604', '2019-10-18 00:27:49.443765', '0', '', '2019101808254238', '3', '1', '5.50', '10.00', '4', '2019101822001419481000068619', '4', '38');

-- ----------------------------
-- Table structure for df_user
-- ----------------------------
DROP TABLE IF EXISTS `df_user`;
CREATE TABLE `df_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `isDelete` tinyint(1) NOT NULL,
  `phone` varchar(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_user
-- ----------------------------
INSERT INTO `df_user` VALUES ('37', 'pbkdf2_sha256$120000$ojWNy0Nf4Gbd$LmFLRi04ofl8OV2u7s996czN3iN/WVQvHeZyCLNn1GM=', null, '0', '1122334455', '', '', '1444587467@qq.com', '0', '0', '2019-09-06 07:16:56.539162', '2019-09-06 07:16:56.632920', '2019-09-06 07:16:56.644862', '0', '');
INSERT INTO `df_user` VALUES ('38', 'pbkdf2_sha256$120000$iY96f2Mn3Jrm$ni4/insYxkrG8El33hvSVvhCN5JknhdRmTX2HDfEvhI=', '2019-10-18 00:14:40.115566', '0', '12345678', '', '', '1444587467@qq.com', '0', '1', '2019-09-12 02:26:58.564440', '2019-09-12 02:26:58.659157', '2019-09-12 02:27:36.682007', '0', '');
INSERT INTO `df_user` VALUES ('39', 'pbkdf2_sha256$120000$j8TO4LWb4dgh$rDx5EbDPiVxKCadx5Wi6z2NhkfVR+WPFoDJknd3vY/c=', '2019-09-18 08:14:53.561280', '1', 'admin', '', '', '', '1', '1', '2019-09-16 07:59:20.171280', '2019-09-16 07:59:20.262071', '2019-09-16 07:59:20.262071', '0', '');

-- ----------------------------
-- Table structure for df_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `df_user_groups`;
CREATE TABLE `df_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `df_user_groups_user_id_group_id_80e5ab91_uniq` (`user_id`,`group_id`),
  KEY `df_user_groups_group_id_36f24e94_fk_auth_group_id` (`group_id`),
  CONSTRAINT `df_user_groups_group_id_36f24e94_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `df_user_groups_user_id_a816b098_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for df_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `df_user_user_permissions`;
CREATE TABLE `df_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `df_user_user_permissions_user_id_permission_id_b22997de_uniq` (`user_id`,`permission_id`),
  KEY `df_user_user_permiss_permission_id_40a6cb2d_fk_auth_perm` (`permission_id`),
  CONSTRAINT `df_user_user_permiss_permission_id_40a6cb2d_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `df_user_user_permissions_user_id_b5f6551b_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of df_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_df_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_df_user_id` FOREIGN KEY (`user_id`) REFERENCES `df_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO `django_admin_log` VALUES ('1', '2019-09-16 12:08:55.492836', '1', 'IndexPromotionBanner object (1)', '1', '[{\"added\": {}}]', '9', '39');
INSERT INTO `django_admin_log` VALUES ('2', '2019-09-16 12:09:37.657561', '2', 'IndexPromotionBanner object (2)', '1', '[{\"added\": {}}]', '9', '39');
INSERT INTO `django_admin_log` VALUES ('3', '2019-09-16 12:12:55.431572', '1', '新鲜水果', '1', '[{\"added\": {}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('4', '2019-09-16 12:13:37.065668', '2', '海鲜水产', '1', '[{\"added\": {}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('5', '2019-09-16 12:13:58.061693', '3', '猪羊牛肉', '1', '[{\"added\": {}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('6', '2019-09-16 12:14:20.430924', '4', '禽类蛋品', '1', '[{\"added\": {}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('7', '2019-09-16 12:14:41.551396', '5', '新鲜蔬菜', '1', '[{\"added\": {}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('8', '2019-09-16 12:15:12.736556', '6', '速冻食品', '1', '[{\"added\": {}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('9', '2019-09-16 12:20:17.423782', '1', 'Goods object (1)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('10', '2019-09-16 12:21:59.434207', '1', 'GoodsSKU object (1)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('11', '2019-09-16 12:22:38.600544', '2', 'Goods object (2)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('12', '2019-09-16 12:23:04.476821', '2', 'GoodsSKU object (2)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('13', '2019-09-16 12:23:46.466195', '3', 'Goods object (3)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('14', '2019-09-16 12:24:17.464859', '3', 'GoodsSKU object (3)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('15', '2019-09-16 12:24:45.155319', '4', 'Goods object (4)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('16', '2019-09-16 12:25:12.634729', '4', 'GoodsSKU object (4)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('17', '2019-09-16 12:27:04.908271', '5', 'Goods object (5)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('18', '2019-09-16 12:27:14.472813', '5', 'Goods object (5)', '2', '[]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('19', '2019-09-16 12:27:46.274546', '5', 'GoodsSKU object (5)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('20', '2019-09-16 12:28:01.145924', '6', 'Goods object (6)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('21', '2019-09-16 12:28:08.490892', '6', 'Goods object (6)', '2', '[]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('22', '2019-09-16 12:28:37.529650', '6', 'GoodsSKU object (6)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('23', '2019-09-16 12:28:51.048705', '7', 'Goods object (7)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('24', '2019-09-16 12:29:11.190098', '7', 'GoodsSKU object (7)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('25', '2019-09-16 12:29:28.817646', '8', 'Goods object (8)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('26', '2019-09-16 12:29:50.013569', '8', 'GoodsSKU object (8)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('27', '2019-09-16 12:30:19.891406', '9', 'Goods object (9)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('28', '2019-09-16 12:31:12.652360', '9', 'GoodsSKU object (9)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('29', '2019-09-16 12:31:36.154516', '10', 'GoodsSKU object (10)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('30', '2019-09-16 12:31:55.223861', '11', 'GoodsSKU object (11)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('31', '2019-09-16 12:32:15.885037', '12', 'GoodsSKU object (12)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('32', '2019-09-16 12:32:42.179441', '13', 'GoodsSKU object (13)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('33', '2019-09-16 12:32:50.942963', '13', 'GoodsSKU object (13)', '2', '[]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('34', '2019-09-16 12:33:09.890299', '14', 'GoodsSKU object (14)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('35', '2019-09-16 12:33:29.342763', '15', 'GoodsSKU object (15)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('36', '2019-09-16 12:33:54.225765', '16', 'GoodsSKU object (16)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('37', '2019-09-16 12:34:12.692482', '17', 'GoodsSKU object (17)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('38', '2019-09-16 12:34:29.743877', '18', 'GoodsSKU object (18)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('39', '2019-09-16 12:34:48.150229', '19', 'GoodsSKU object (19)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('40', '2019-09-16 12:35:05.752004', '20', 'GoodsSKU object (20)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('41', '2019-09-16 12:35:24.959666', '21', 'GoodsSKU object (21)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('42', '2019-09-16 12:35:42.518782', '22', 'GoodsSKU object (22)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('43', '2019-09-16 12:36:00.139818', '23', 'GoodsSKU object (23)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('44', '2019-09-16 12:36:18.819938', '24', 'GoodsSKU object (24)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('45', '2019-09-16 12:39:12.242491', '1', 'IndexTypeGoodsBanner object (1)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('46', '2019-09-16 12:39:26.419581', '2', 'IndexTypeGoodsBanner object (2)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('47', '2019-09-16 12:39:34.436060', '3', 'IndexTypeGoodsBanner object (3)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('48', '2019-09-16 12:39:46.510230', '4', 'IndexTypeGoodsBanner object (4)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('49', '2019-09-16 12:40:34.028106', '4', 'IndexTypeGoodsBanner object (4)', '3', '', '10', '39');
INSERT INTO `django_admin_log` VALUES ('50', '2019-09-16 12:40:42.668677', '3', 'IndexTypeGoodsBanner object (3)', '3', '', '10', '39');
INSERT INTO `django_admin_log` VALUES ('51', '2019-09-16 12:40:42.674681', '2', 'IndexTypeGoodsBanner object (2)', '3', '', '10', '39');
INSERT INTO `django_admin_log` VALUES ('52', '2019-09-16 12:40:42.677652', '1', 'IndexTypeGoodsBanner object (1)', '3', '', '10', '39');
INSERT INTO `django_admin_log` VALUES ('53', '2019-09-16 12:40:51.053454', '5', 'IndexTypeGoodsBanner object (5)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('54', '2019-09-16 12:40:58.893039', '6', 'IndexTypeGoodsBanner object (6)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('55', '2019-09-16 12:41:05.420524', '7', 'IndexTypeGoodsBanner object (7)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('56', '2019-09-16 12:41:14.437486', '8', 'IndexTypeGoodsBanner object (8)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('57', '2019-09-16 12:41:29.323077', '9', 'IndexTypeGoodsBanner object (9)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('58', '2019-09-16 12:41:37.933173', '10', 'IndexTypeGoodsBanner object (10)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('59', '2019-09-16 12:41:46.379633', '11', 'IndexTypeGoodsBanner object (11)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('60', '2019-09-16 12:41:52.697092', '12', 'IndexTypeGoodsBanner object (12)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('61', '2019-09-16 12:42:03.867611', '13', 'IndexTypeGoodsBanner object (13)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('62', '2019-09-16 12:42:10.412763', '14', 'IndexTypeGoodsBanner object (14)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('63', '2019-09-16 12:42:19.284434', '15', 'IndexTypeGoodsBanner object (15)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('64', '2019-09-16 12:42:51.400179', '16', 'IndexTypeGoodsBanner object (16)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('65', '2019-09-16 12:43:00.148521', '17', 'IndexTypeGoodsBanner object (17)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('66', '2019-09-16 12:43:09.705413', '18', 'IndexTypeGoodsBanner object (18)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('67', '2019-09-16 12:43:16.597038', '19', 'IndexTypeGoodsBanner object (19)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('68', '2019-09-16 12:43:24.279246', '20', 'IndexTypeGoodsBanner object (20)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('69', '2019-09-16 12:43:43.449347', '21', 'IndexTypeGoodsBanner object (21)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('70', '2019-09-16 12:43:49.808992', '22', 'IndexTypeGoodsBanner object (22)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('71', '2019-09-16 12:43:57.275671', '23', 'IndexTypeGoodsBanner object (23)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('72', '2019-09-16 12:44:04.430544', '24', 'IndexTypeGoodsBanner object (24)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('73', '2019-09-16 12:44:27.077343', '25', 'IndexTypeGoodsBanner object (25)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('74', '2019-09-16 12:44:34.304118', '26', 'IndexTypeGoodsBanner object (26)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('75', '2019-09-16 12:44:42.032472', '27', 'IndexTypeGoodsBanner object (27)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('76', '2019-09-16 12:44:49.613922', '28', 'IndexTypeGoodsBanner object (28)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('77', '2019-09-16 12:48:05.260148', '1', 'IndexGoodsBanner object (1)', '1', '[{\"added\": {}}]', '14', '39');
INSERT INTO `django_admin_log` VALUES ('78', '2019-09-16 12:48:31.835638', '2', 'IndexGoodsBanner object (2)', '1', '[{\"added\": {}}]', '14', '39');
INSERT INTO `django_admin_log` VALUES ('79', '2019-09-16 12:49:05.835970', '3', 'IndexGoodsBanner object (3)', '1', '[{\"added\": {}}]', '14', '39');
INSERT INTO `django_admin_log` VALUES ('80', '2019-09-16 12:49:29.423176', '4', 'IndexGoodsBanner object (4)', '1', '[{\"added\": {}}]', '14', '39');
INSERT INTO `django_admin_log` VALUES ('81', '2019-09-17 00:03:04.567155', '10', 'Goods object (10)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('82', '2019-09-17 00:03:52.394064', '25', 'GoodsSKU object (25)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('83', '2019-09-17 00:03:55.872898', '29', 'IndexTypeGoodsBanner object (29)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('84', '2019-09-17 00:05:18.038711', '11', 'Goods object (11)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('85', '2019-09-17 00:05:40.318429', '26', 'GoodsSKU object (26)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('86', '2019-09-17 00:05:50.728843', '30', 'IndexTypeGoodsBanner object (30)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('87', '2019-09-17 00:05:57.456848', '31', 'IndexTypeGoodsBanner object (31)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('88', '2019-09-17 00:06:17.507355', '31', 'IndexTypeGoodsBanner object (31)', '3', '', '10', '39');
INSERT INTO `django_admin_log` VALUES ('89', '2019-09-17 00:10:44.671154', '1', '新鲜水果', '2', '[{\"changed\": {\"fields\": [\"logo\"]}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('90', '2019-09-17 00:10:57.989404', '2', '海鲜水产', '2', '[{\"changed\": {\"fields\": [\"logo\"]}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('91', '2019-09-17 00:11:03.661474', '3', '猪羊牛肉', '2', '[{\"changed\": {\"fields\": [\"logo\"]}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('92', '2019-09-17 00:11:07.757032', '4', '禽类蛋品', '2', '[{\"changed\": {\"fields\": [\"logo\"]}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('93', '2019-09-17 00:11:21.461274', '5', '新鲜蔬菜', '2', '[{\"changed\": {\"fields\": [\"logo\"]}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('94', '2019-09-17 00:11:33.762403', '6', '速冻食品', '2', '[{\"changed\": {\"fields\": [\"logo\"]}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('95', '2019-09-17 00:11:54.323206', '1', '新鲜水果', '2', '[{\"changed\": {\"fields\": [\"logo\"]}}]', '15', '39');
INSERT INTO `django_admin_log` VALUES ('96', '2019-09-17 00:15:22.974708', '12', 'Goods object (12)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('97', '2019-09-17 00:15:48.567433', '27', 'GoodsSKU object (27)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('98', '2019-09-17 00:16:05.504667', '32', 'IndexTypeGoodsBanner object (32)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('99', '2019-09-17 00:16:14.137598', '32', 'IndexTypeGoodsBanner object (32)', '2', '[{\"changed\": {\"fields\": [\"display_type\"]}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('100', '2019-09-17 00:17:03.030422', '13', 'Goods object (13)', '1', '[{\"added\": {}}]', '12', '39');
INSERT INTO `django_admin_log` VALUES ('101', '2019-09-17 00:17:33.293933', '28', 'GoodsSKU object (28)', '1', '[{\"added\": {}}]', '13', '39');
INSERT INTO `django_admin_log` VALUES ('102', '2019-09-17 00:17:36.065150', '33', 'IndexTypeGoodsBanner object (33)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('103', '2019-09-17 00:17:50.191672', '34', 'IndexTypeGoodsBanner object (34)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('104', '2019-09-17 00:18:38.191124', '35', 'IndexTypeGoodsBanner object (35)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('105', '2019-09-17 00:18:45.973103', '36', 'IndexTypeGoodsBanner object (36)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('106', '2019-09-17 00:18:53.271608', '37', 'IndexTypeGoodsBanner object (37)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('107', '2019-09-17 00:19:01.420357', '38', 'IndexTypeGoodsBanner object (38)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('108', '2019-09-17 00:19:08.857996', '39', 'IndexTypeGoodsBanner object (39)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('109', '2019-09-17 00:19:20.677976', '40', 'IndexTypeGoodsBanner object (40)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('110', '2019-09-17 00:19:27.493096', '41', 'IndexTypeGoodsBanner object (41)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('111', '2019-09-17 00:19:35.971532', '42', 'IndexTypeGoodsBanner object (42)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('112', '2019-09-17 00:19:42.506336', '43', 'IndexTypeGoodsBanner object (43)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('113', '2019-09-17 00:19:48.981795', '44', 'IndexTypeGoodsBanner object (44)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('114', '2019-09-17 00:19:57.931717', '45', 'IndexTypeGoodsBanner object (45)', '1', '[{\"added\": {}}]', '10', '39');
INSERT INTO `django_admin_log` VALUES ('115', '2019-09-17 00:20:04.849218', '46', 'IndexTypeGoodsBanner object (46)', '1', '[{\"added\": {}}]', '10', '39');

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES ('1', 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES ('3', 'auth', 'group');
INSERT INTO `django_content_type` VALUES ('2', 'auth', 'permission');
INSERT INTO `django_content_type` VALUES ('7', 'captcha', 'captchastore');
INSERT INTO `django_content_type` VALUES ('4', 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES ('12', 'fresh_goods', 'goods');
INSERT INTO `django_content_type` VALUES ('11', 'fresh_goods', 'goodsimage');
INSERT INTO `django_content_type` VALUES ('13', 'fresh_goods', 'goodssku');
INSERT INTO `django_content_type` VALUES ('15', 'fresh_goods', 'goodstype');
INSERT INTO `django_content_type` VALUES ('14', 'fresh_goods', 'indexgoodsbanner');
INSERT INTO `django_content_type` VALUES ('9', 'fresh_goods', 'indexpromotionbanner');
INSERT INTO `django_content_type` VALUES ('10', 'fresh_goods', 'indextypegoodsbanner');
INSERT INTO `django_content_type` VALUES ('17', 'fresh_order', 'ordergoods');
INSERT INTO `django_content_type` VALUES ('16', 'fresh_order', 'orderinfo');
INSERT INTO `django_content_type` VALUES ('8', 'fresh_user', 'address');
INSERT INTO `django_content_type` VALUES ('6', 'fresh_user', 'user');
INSERT INTO `django_content_type` VALUES ('5', 'sessions', 'session');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES ('1', 'contenttypes', '0001_initial', '2019-08-13 01:08:59.047324');
INSERT INTO `django_migrations` VALUES ('2', 'contenttypes', '0002_remove_content_type_name', '2019-08-13 01:08:59.148971');
INSERT INTO `django_migrations` VALUES ('3', 'auth', '0001_initial', '2019-08-13 01:08:59.474845');
INSERT INTO `django_migrations` VALUES ('4', 'auth', '0002_alter_permission_name_max_length', '2019-08-13 01:08:59.534362');
INSERT INTO `django_migrations` VALUES ('5', 'auth', '0003_alter_user_email_max_length', '2019-08-13 01:08:59.547756');
INSERT INTO `django_migrations` VALUES ('6', 'auth', '0004_alter_user_username_opts', '2019-08-13 01:08:59.556216');
INSERT INTO `django_migrations` VALUES ('7', 'auth', '0005_alter_user_last_login_null', '2019-08-13 01:08:59.568586');
INSERT INTO `django_migrations` VALUES ('8', 'auth', '0006_require_contenttypes_0002', '2019-08-13 01:08:59.572559');
INSERT INTO `django_migrations` VALUES ('9', 'auth', '0007_alter_validators_add_error_messages', '2019-08-13 01:08:59.580987');
INSERT INTO `django_migrations` VALUES ('10', 'auth', '0008_alter_user_username_max_length', '2019-08-13 01:08:59.591448');
INSERT INTO `django_migrations` VALUES ('11', 'auth', '0009_alter_user_last_name_max_length', '2019-08-13 01:08:59.599834');
INSERT INTO `django_migrations` VALUES ('12', 'fresh_user', '0001_initial', '2019-08-13 01:08:59.964997');
INSERT INTO `django_migrations` VALUES ('13', 'admin', '0001_initial', '2019-08-13 01:09:00.121538');
INSERT INTO `django_migrations` VALUES ('14', 'admin', '0002_logentry_remove_auto_add', '2019-08-13 01:09:00.132935');
INSERT INTO `django_migrations` VALUES ('15', 'admin', '0003_logentry_add_action_flag_choices', '2019-08-13 01:09:00.143386');
INSERT INTO `django_migrations` VALUES ('16', 'sessions', '0001_initial', '2019-08-13 01:09:00.190248');
INSERT INTO `django_migrations` VALUES ('17', 'fresh_user', '0002_user_phone', '2019-09-05 00:37:35.336075');
INSERT INTO `django_migrations` VALUES ('18', 'captcha', '0001_initial', '2019-09-05 06:24:54.956081');
INSERT INTO `django_migrations` VALUES ('19', 'fresh_goods', '0001_initial', '2019-09-10 09:02:10.716836');
INSERT INTO `django_migrations` VALUES ('20', 'fresh_user', '0003_address', '2019-09-10 09:02:10.842592');
INSERT INTO `django_migrations` VALUES ('21', 'fresh_order', '0001_initial', '2019-09-10 09:02:11.613129');
INSERT INTO `django_migrations` VALUES ('22', 'fresh_order', '0002_auto_20190924_1056', '2019-09-24 02:56:24.387445');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('n7vc5z4pe77u8nu0fx1f23xihfy86tqt', 'OWQzZDUyZTEzYWNlNjA0MjdmYmRjOWYwZjgzYTM3M2NkNTNlM2ZkMjp7Il9hdXRoX3VzZXJfaWQiOiIyMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZGJlMjRiYWE4ZGYyM2EyM2FjYmNmMDZjYWRjNGE2YWVlNTA2NzI5ZSJ9', '2019-08-31 03:12:10.986393');
