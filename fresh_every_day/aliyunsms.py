'''
使用北网账号发送短信完整代码
前提安装阿里云sdk
pip install aliyun-python-sdk-core
pip install aliyun-python-sdk-core-v3 #新版,支持python3.x的
'''
'''
短信下发核心方法
1. send_sms 发短信
2. get_code 生成验证码
'''
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest
import random
#短信验证
def send_sms(phone,code):
    #阿里云的短信接口密钥
    client = AcsClient('LTAI4FvyEgy1jT3jwCHmN5Dx', 'vBPu9RWeB87d2zhFMAth1AwBh4YgHU', 'cn-hangzhou')

    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', phone)
    request.add_query_param('SignName', "Chico")
    request.add_query_param('TemplateCode', "SMS_173476924")
    request.add_query_param('TemplateParam', '{code:%s}'%code)

    response = client.do_action(request)
    # python2:  print(response)
    print(str(response, encoding='utf-8'))
    return str(response,encoding='utf-8')

#生成随机验证码
def get_code(n=6,alpha=True):
    s='' #创建字符串变量，存储生成的验证码
    for i in range(n):
        num=random.randint(0,9) #生成随机数字0-9
        if alpha: #需要字母验证码，不能传参，如果不需要字母，关键字alpha=False
            #运用chr方法 ASCII码
            upper_alpha=chr(random.randint(65,90))
            lower_alpha=chr(random.randint(97,122))
            num=random.choice([num,upper_alpha,lower_alpha])
        s=s+str(num)
    return s

if __name__ == '__main__':
    # 调用下发短信方法
    send_sms('18636350081', get_code(6,False))
    print(get_code(6,False)) # 打印6位数字验证码
    print(get_code(6,True)) # 打印6位数字字母混合验证码