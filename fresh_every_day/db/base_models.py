from django.db import models

class BaseModel(models.Model):
    """抽象模型基类"""
    create_time=models.DateTimeField(auto_now_add=True,verbose_name='创建时间')
    update_time=models.DateTimeField(auto_now=True,verbose_name='更新时间')
    isDelete=models.BooleanField(default=False,verbose_name='删除标记')
    phone = models.CharField(max_length=11, verbose_name='联系电话')

    class Meta:
        """表示为抽象模型类"""
        abstract=True