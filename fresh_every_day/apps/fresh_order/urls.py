from django.conf.urls import url,include
from .views import *
urlpatterns = [
    url(r'^place$',OrderPlaceView.as_view(),name='place'),#提交订单页面
    url(r'^commit$',OrderCommitView.as_view(),name='commit'), #订单创建
    url('^pay$',OrderPayView.as_view(),name='pay'),#订单支付
    url(r'^check$',OrderCheckView.as_view(),name='check'), #检验订单是否支付
]