from django.apps import AppConfig


class FreshOrderConfig(AppConfig):
    name = 'fresh_order'
