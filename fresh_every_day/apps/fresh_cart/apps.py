from django.apps import AppConfig


class FreshCartConfig(AppConfig):
    name = 'fresh_cart'
