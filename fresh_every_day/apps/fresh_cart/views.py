from django.shortcuts import render
from django.http import *
from django.views.generic import View
from fresh_goods.models import *
from django_redis import get_redis_connection
from utlis.mixin import *
# Create your views here.


#购物车添加记录
class CartAddView(View):
    def post(self,request):
        user=request.user
        #判断用户是否登陆
        if not user.is_authenticated:
            #用户未登录
            return JsonResponse({'res':0,'errmsg':'请先登录'})
        #购物车记录添加
        #接受数据
        sku_id=request.POST.get('sku_id')
        count=request.POST.get('count')
        #数据校验
        if not all([sku_id,count]):
            return  JsonResponse({'res':1,'errmsg':'数据不完整'})
        #校验添加的商品数量
        try:
            count=int(count)
        except Exception as e:
            return JsonResponse({'res':2,'errmsg':'商品数目出错'})
        #校验商品是否存在
        try:
            sku=GoodsSKU.objects.get(id=sku_id)
        except GoodsSKU.DoesNotExist:
            #商品不存在
            return JsonResponse({'res':3,'errmsg':'商品不存在' })
        #业务处理：添加购物车记录
        conn=get_redis_connection('default')
        cart_key='cart_%d'%user.id
        # 先尝试获取sku_id的值 hget(key,属性) --cart_key,属性
        #如果sku_id在hash中不存在，hget返回None
        cart_count=conn.hget(cart_key,sku_id)
        if cart_count:
            #累加购物车中商品的数目
            count+=int(cart_count)
        #校验商品的库存
        if count>sku.stock:
            return JsonResponse({'res':4,'errmsg':'商品库存不足'})
        #设置hash中sku_id对应的值
        #hset(key,属性,属性值) -- 如果存在是更新，如果不存在是新增
        conn.hset(cart_key,sku_id,count)
        #计算购物车中商品条目数
        total_count=conn.hlen(cart_key)
        #返回应答
        return  JsonResponse({'res':5,'message':'添加成功','total_count':total_count})


#/cart
#我的购物车
class CartInfoView(LoginRequiredMixin,View):
    #购物车页面显示
    def get(self,request):
        #显示
        #获取登录的用户
        user=request.user
        #获取用户购物车中商品信息
        conn=get_redis_connection('default')
        cart_key='cart_%d'%user.id
        #hgetall(name) 根据key获取里面所有的值，返回一个dict格式
        #格式--{'商品id':'商品数量'}
        cart_dict=conn.hgetall(cart_key)

        #遍历获取商品的信息
        skus=[]
        #保存购物车中商品的总价和总数
        total_count=0
        total_price=0
        for sku_id,count in cart_dict.items():
            #根据商品的id获取商品信息
            sku=GoodsSKU.objects.get(id=sku_id)
            #计算商品的小计
            amount=sku.price*int(count)
            print(sku.price,int(count))
            #动态给sku对象增加属性 amount 保存商品的小计
            sku.amount=amount
            #动态给sku对象增加属性 count 保存购物车中对应商品的数量
            sku.count=int(count)
            skus.append(sku)
            #重新计算商品的总数目和总价格
            total_count+=int(count)
            total_price+=amount
        return render(request,'cart.html',locals())


#更新购物车记录
#采用ajax post请求
#前端需要传递的参数 -- 商品id(sku_id) 更新商品的数量(count)
#/cart/update
class CartUpdateView(View):
    def post(self,request):
        #购物车记录更新
        user = request.user
        # 判断用户是否登陆
        if not user.is_authenticated:
            # 用户未登录
            return JsonResponse({'res': 0, 'errmsg': '请先登录'})
        # 接受数据
        sku_id = request.POST.get('sku_id')
        count = request.POST.get('count')
         # 数据校验
        if not all([sku_id, count]):
            return JsonResponse({'res': 1, 'errmsg': '数据不完整'})
        # 校验添加的商品数量
        try:
            count = int(count)
        except Exception as e:
            return JsonResponse({'res': 2, 'errmsg': '商品数目出错'})
        # 校验商品是否存在
        try:
            sku = GoodsSKU.objects.get(id=sku_id)
        except GoodsSKU.DoesNotExist:
            # 商品不存在
            return JsonResponse({'res': 3, 'errmsg': '商品不存在'})
        #业务处理 -- 更新购物车记录
        conn=get_redis_connection('default')
        cart_key='cart_%d'%user.id
        #校验商品的库存
        if count > sku.stock:
            return JsonResponse({'res':4 , 'errmsg':'商品库存不足'})
        #更新
        conn.hset(cart_key,sku_id,count)
        #计算用户购物车中商品的总件数 {'1':5,'2':3}   -- 5+3
        #hvals(key)根据key值返回里面的所有values 是一个list形式
        total_count=0
        vals=conn.hvals(cart_key)
        for val in vals:
            total_count+=int(val)
        #返回页面
        return  JsonResponse({'res':5 , 'message':'更新成功','total_count':total_count})

#删除购物车记录
# 采用ajax post请求
#前端需要传递的参数：商品的id(sku_id)
#/cart/delete
class CartDeleteView(View):
    def post(self,request):
        #购物车记录删除
        user = request.user
        # 判断用户是否登陆
        if not user.is_authenticated:
            # 用户未登录
            return JsonResponse({'res': 0, 'errmsg': '请先登录'})
        #接收参数
        sku_id=request.POST.get('sku_id')

        #数据的校验
        if not sku_id:
            return JsonResponse({'res':1,'errmsg':'无效的商品id'})
        #校验商品是否存在
        try:
            sku=GoodsSKU.objects.get(id=sku_id)
        except GoodsSKU.DoesNotExist:
            #商品不存在
            return JsonResponse({'res':2,'errmsg':'商品不存在'})

        #业务处理 删除购物车记录
        conn=get_redis_connection('default')
        cart_key='cart_%d'%user.id

        #删除记录
        #hdel(name,keys) 可以删除对应key中的一些值 可以自己指定
        conn.hdel(cart_key,sku_id)
        #获取商品总件数
        total_count = 0
        vals = conn.hvals(cart_key)
        for val in vals:
            total_count += int(val)
        #返回英法
        return  JsonResponse({'res':3,'message':'删除成功','total_count':total_count})
