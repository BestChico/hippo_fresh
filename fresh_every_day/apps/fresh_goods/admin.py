from django.contrib import admin
from .models import *
from django.core.cache import cache
# Register your models here.
#定义修改或删除首页静态页的父类
class BaseModelAdmin(admin.ModelAdmin):
    # 我们不能调用save_model方法 就要对这个方法进行重写
    def save_model(self, request, obj, form, change):
        # 新增或者更新表中的数据式调用
        # 不影响父类的新增或是更新的方法
        super().save_model(request, obj, form, change)
        # 增加附加操作
        # 发出任务 让celery worker 重新生成首页静态页
        from celery_tasks.tasks import generate_static_index_html
        generate_static_index_html.delay()

        #清除首页缓存数据
        cache.delete('index_page_data')

    # 对删除model方法进行重写
    def delete_model(self, request, obj):
        # 删除表中数据时调用
        super().delete_model(request, obj)
        # 增加附加操作
        # 发出任务 让celery worker 重新生成首页静态页
        from celery_tasks.tasks import generate_static_index_html
        generate_static_index_html.delay()

        # 清除首页缓存数据
        cache.delete('index_page_data')
#让下面的方法都继承BaseModelAdmin方法--重写首页静态页
class IndexPromotionBannerAdmin(BaseModelAdmin):
    pass
class GoodsTypeAdmin(BaseModelAdmin):
    pass
class GoodsSKUAdmin(BaseModelAdmin):
    pass
class GoodsAdmin(BaseModelAdmin):
    pass
class GoodsImageAdmin(BaseModelAdmin):
    pass
class IndexGoodsBannerAdmin(BaseModelAdmin):
    pass
class IndexTypeGoodsBannerAdmin(BaseModelAdmin):
    pass



admin.site.register(GoodsType,GoodsTypeAdmin)
admin.site.register(GoodsSKU,GoodsSKUAdmin)
admin.site.register(Goods,GoodsAdmin)
admin.site.register(GoodsImage,GoodsImageAdmin)
admin.site.register(IndexGoodsBanner,IndexGoodsBannerAdmin)
admin.site.register(IndexTypeGoodsBanner,IndexTypeGoodsBannerAdmin)
admin.site.register(IndexPromotionBanner,IndexPromotionBannerAdmin)
#注册后台管理员提交后自动生成静态首页

