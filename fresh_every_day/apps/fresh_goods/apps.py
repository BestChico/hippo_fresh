from django.apps import AppConfig


class FreshGoodsConfig(AppConfig):
    name = 'fresh_goods'
