from django import forms
from .models import *
from captcha.fields import CaptchaField


class RegisterFrom(forms.Form):
    class Meta:
        models = User
        fileld = ['emali']

    emali = forms.EmailField(required=True, error_messages={'invalid': '邮箱格式不正确'})
    phone = forms.IntegerField(required=True, min_value=11, max_value=11,
                               error_messages={'max_length': '最大为11个字符', 'min_length': '最小为11个字符'})

    # 邮箱验证
    def clean_emali(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('邮箱已注册')
        return email

    # 电话验证
    def clean_phone(self):
        phone = self.cleaned_data.get('phone')
        try:
            user = User.objects.get(phone=phone)
        except User.DoesNotExist:
            user = None
        # 说明数据库有重名的
        if user is not None:
            raise forms.ValidationError('手机号已存在')
        return user


class LoginForm(forms.Form):
    # captcha = CaptchaField(label='验证码', error_messages={'invalid': '验证码错误'})  # 生称随机的验证码图片
    captcha = CaptchaField(label='验证码', error_messages={'invalid': '验证码错误'})  # 生称随机的验证码图片
